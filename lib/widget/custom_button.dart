import 'package:flutter/cupertino.dart';
import 'dart:io';

import 'package:flutter/material.dart';

import 'colorCustom.dart';
//import 'package:splash/widget/colorCustom.dart';

class CustomButton extends StatelessWidget {
  CustomButton({@required this.onPresed,@required this.text});
  final GestureTapCallback onPresed;
  final String text;



  @override
  Widget build(BuildContext context) {
    return Container(
      height: Platform.isIOS ? 62 : 56,
      child: RawMaterialButton(
        child: Container(
          decoration: BoxDecoration(
            color: customColor().logoColor,
            // gradient: LinearGradient(colors: [customColor().barColor, customColor().bkColor],
            //   begin: Alignment.centerLeft,
            //   end: Alignment.centerRight,
            // ),
            //   border: Border.all(
            //       color: Colors.grey,
            //       style: BorderStyle.solid,
            //       width: 1.0),

              borderRadius: BorderRadius.circular(40.0)),
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        onPressed: onPresed,
        ),
    );

  }
}
