import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:splash/utils/my_navigator.dart';

import 'colorCustom.dart';

class HomeTab extends StatelessWidget {


  HomeTab({@required this.onPresed,@required this.text,@required this.desc,@required this.colorst,@required this.colored, this.icon, this.image});
  final GestureTapCallback onPresed;
  final String text, desc;
  final Color colorst, colored;
  final IconData icon;
  final AssetImage image;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child:  RawMaterialButton(
        child: Container(
          height: 100,
          width: 140,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [colorst, colored],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
              border: Border.all(
                  color: Colors.grey,
                  style: BorderStyle.solid,
                 ),

              borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10.0,),
              Icon(icon,color: Colors.white,),
              //ImageIcon(image,color: Colors.white,),
              Text(text,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.white,
                    fontFamily: "Montserrat"
                ),
              ),
              SizedBox(height: 2.0,),
              Text(desc,
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontFamily: "Montserrat"
                ),
              ),
            ],
          ),
        ),
        onPressed: onPresed,
      ),

    );
  }
}
