import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:okr/home/search_result.dart';
import 'package:okr/services/models/OKRMaster.dart';
import 'package:okr/utils/my_navigator.dart';
// import 'package:http/http.dart' as http;
//
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/services/appconstant.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/notificationMaster.dart';
// import 'package:splash/utils/my_navigator.dart';

import 'colorCustom.dart';
class CustomAppBar extends StatefulWidget {
  CustomAppBar({ this.text, this.image});
  final String text;
  final String image;
  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  int counter = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
     // notificationCount();
    });
  }
  @override
  Widget build(BuildContext context) {
    return
      AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
          color: customColor().logoColor
          ),
        ),
        actions: <Widget>[
          // PopupMenuButton(
          //   itemBuilder: (BuildContext bc) => [
          //     PopupMenuItem(child: Text("Favourite"), value: "fav",),
          //     PopupMenuItem(
          //         child: Text("Profile"), value: "pro"),
          //     PopupMenuItem(child: Text("Logout"), value: "/settings"),
          //   ],
          //   onSelected: (route) {
          //     print(route);
          //     if(route == "pro" )
          //       {
          //         MyNavigator.goToMyProfile(context);
          //       }
          //     else if(route == "fav" )
          //     {
          //       MyNavigator.goToBookmark(context);
          //       // String head = "Favorites";
          //       // OKRMaster okrMasterTemp = new OKRMaster(header: head);
          //       // Navigator.push(
          //       //   context,
          //       //   MaterialPageRoute(
          //       //     builder: (_) => SearchResult(
          //       //       okrMaster: okrMasterTemp,
          //       //     ),),);
          //     }
          //     // Note You must create respective pages for navigation
          //     Navigator.pushNamed(context, route);
          //   },
          // ),
        ],
        title: Center(child: Text(widget.text,style: TextStyle(color: Colors.white,fontFamily: 'Montserrat',fontSize: 20, fontWeight: FontWeight.bold,),)),

        iconTheme: new IconThemeData(color: Colors.white),

      );
  }


// void notificationCount() async{
//   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//   String token = sharedPreferences.getString("token");
//   int userId = sharedPreferences.getInt("id");
//   User userTemp = new User();
//   userTemp.setId(userId);
//   NotificationMaster notificationMaster = NotificationMaster();
//   notificationMaster.setCreatedFor(userTemp);
//   var body = json.encode(notificationMaster);
//   var jsonResponse;
//   var response = await http.post(AppConstants.countNotification_URL,
//       headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
//       body: body);
//   if (response.statusCode == 200) {
//     jsonResponse = response.body;
//     counter = int.parse(jsonResponse);
//     setState(() {
//
//     });
//   }
// }
}


//class CustomAppBar extends StatelessWidget {
//  CustomAppBar({ this.text, this.image});
////  final String text, image;
//  @override
//  Widget build(BuildContext context) {
//    return
//       AppBar(
//        flexibleSpace: Container(
//          decoration: BoxDecoration(
//            gradient: LinearGradient(colors: [customColor().barColor, customColor().bkColor],
//              begin: Alignment.centerLeft,
//              end: Alignment.centerRight,
//            ),
//          ),
//        ),
//        actions: <Widget>[
//          IconButton(
//            onPressed: (){
//              MyNavigator.goToNotificationScreen(context);
//            },
//            icon: Icon(Icons.notifications_none, ),
//
//          ),
//        ],
//        title: Center(child: Text(text,style: TextStyle(color: Colors.white,fontFamily: 'Montserrat',fontSize: 20, fontWeight: FontWeight.bold,),)),
//
//        iconTheme: new IconThemeData(color: Colors.white),
//
//    );
//  }
//}




//  @override
//  Widget build(BuildContext context) {
//    return AppBar(
//      flexibleSpace: Container(
//        decoration: BoxDecoration(
//          gradient: LinearGradient(colors: [customColor().barColor, customColor().bkColor],
//            begin: Alignment.centerLeft,
//            end: Alignment.centerRight,
//          ),
//        ),
//      ),
//      actions: <Widget>[
//        IconButton(
//          onPressed: (){
//            MyNavigator.goToNotificationScreen(context);
//          },
//          icon: Icon(Icons.notifications_none, ),
//
//        ),
//      ],
//      title: Center(child: Text(text,style: TextStyle(color: Colors.white),)),
//      iconTheme: new IconThemeData(color: Colors.white),
//    );
//  }
//}