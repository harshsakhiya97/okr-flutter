
import 'package:flutter/material.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/utils/logout.dart';
import 'package:okr/utils/my_navigator.dart';

import 'package:shared_preferences/shared_preferences.dart';


import 'colorCustom.dart';
class AppDrawer extends StatefulWidget {
  @override
  AppDrawerState createState() => AppDrawerState();
}

class AppDrawerState extends State<AppDrawer> {
  String title = "",
      email = "";
  User user;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadname();
    loadSharedPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(

            accountName: Text(
              title.toString(), style: TextStyle(color: Colors.white),),
            accountEmail: Text(
              email.toString(), style: TextStyle(color: Colors.white),),
            decoration: BoxDecoration(
                color: customColor().logoColor

            ),
          ),
          ListTile(
            title: Text(
              "Home", style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.home, color: customColor().logoColor,),
            onTap: () {
              Navigator.of(context).pop();
              MyNavigator.goToBottom(context);
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              "Favourite", style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.star, color: customColor().logoColor,),
            onTap: () {
              Navigator.of(context).pop();
              MyNavigator.goToBookmark(context);
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              "My Profile", style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.account_circle, color: customColor().logoColor,),
            onTap: () {
              Navigator.of(context).pop();
              MyNavigator.goToMyProfile(context);
            },
          ),
          Divider(),
          ListTile(
            title: Text("Change Password",
              style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.adjust, color: customColor().logoColor,),
            onTap: () {
              Navigator.of(context).pop();
              MyNavigator.goToChangePassword(context);
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              "About us", style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.account_box, color: customColor().logoColor,),
            onTap: () {
              Navigator.of(context).pop();
              MyNavigator.goToAboutUs(context);
            },
          ),
          Divider(),

          // ListTile(
          //   title: Text("Synchronise",style: TextStyle(color: customColor().bkColor),),
          //   trailing: Icon(Icons.sync,color: customColor().bkColor,),
          //   onTap: () async {
          //     loadBookmark();
          //   //  await Future.delayed(const Duration(seconds: 2));
          //     AllTestApi().getfilter(pr);
          //     AllTestApi().getCategory();
          //     AllTestApi().getSubCategory();
          //   },
          // ),
          // Divider(),
          ListTile(
            title: Text(
              "Logout", style: TextStyle(fontSize: 12 ,color: customColor().barColor),),
            trailing: Icon(Icons.settings_power, color: customColor().logoColor,),
            onTap: () {
              showDialog(
                context: context,
                builder: (_) => LogoutOverlay(),
              );
            },
          ),

        ],
      ),
    );
  }

  void loadname() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      title = "Atul Mishra";
      email = "am@gmail.com";
      // title = sharedPreferences.getString("firstName").toString();
      // email = sharedPreferences.getString("userEmail").toString();
    });
  }


  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        if (user != null) {
          title = user.firstName.toString();
          email = user.email.toString();
        }
        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }
}


//class AppDrawer extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
