import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colorCustom.dart';

class CustomBox extends StatelessWidget {
  bool valuefirst = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
            style: BorderStyle.solid,
          ),

          borderRadius: BorderRadius.circular(10.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Checkbox(
              checkColor: Colors.white,
              activeColor: customColor().logoColor,
              value: this.valuefirst,
              onChanged: (bool value) {

                  this.valuefirst = value;
                }
                ),
            Expanded(
              flex: 1,
              child: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("O :   Title of Okr",
                  style: GoogleFonts.montserrat(
                    fontSize: 16,
                    color: customColor().barColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                  SizedBox(height: 10,),
                  Text("KR : Category of Okr",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      color: customColor().barColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text("KR : Subcategory of Okr",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      color: customColor().barColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Container(child: IconButton(icon: Icon(Icons.star_border, color: customColor().logoColor), onPressed: (){})),
          ],
        ),
      ),
    );
  }
}
