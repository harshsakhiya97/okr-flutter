import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colorCustom.dart';

class CustomSave extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
            style: BorderStyle.solid,
          ),

          borderRadius: BorderRadius.circular(10.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Keyword",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      color: customColor().barColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text("Category",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      color: customColor().barColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text("Subcategory",
                    style: GoogleFonts.montserrat(
                      fontSize: 16,
                      color: customColor().barColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 5, left: 3),
                  child: Container(
                    decoration: BoxDecoration(
                      color: customColor().logoColor,
                        // border: Border.all(
                        //   style: BorderStyle.solid,
                        // ),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("20",
                        style: GoogleFonts.montserrat(
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(child: IconButton(icon: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Icon(Icons.delete_forever_outlined, color: customColor().logoColor),
                ), onPressed: (){})),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
