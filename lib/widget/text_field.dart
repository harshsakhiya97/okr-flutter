import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class AMTextField extends StatelessWidget {
  AMTextField({@required this.text, this.keyboardType,this.validator, this.onSaved, this.onFieldSubmitted, this.controller, @required this.obscureText, this.readOnly,});
  final String text;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final  TextInputType keyboardType;
  final TextEditingController controller;
  final bool obscureText, readOnly ;

  @override
  Widget build(BuildContext context) {

    return  TextFormField(
      decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius:  BorderRadius.all(
              const Radius.circular(40.0),
            ),
            borderSide: BorderSide( style: BorderStyle.solid,color: Colors.grey, ),
          ),
          hintStyle: TextStyle(
            fontSize: 12,
            fontFamily: 'Montserrat',
            color: Colors.grey,
          ),
          hintText: text,
          fillColor: Colors.white),
      controller: controller,
      keyboardType: keyboardType,
      readOnly: readOnly,
      validator: validator,
//      validator: validator(value){
////        Pattern pattern =
////            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
////        RegExp regex = new RegExp(pattern);
////        if (!regex.hasMatch(value))
////          return 'Enter Valid Email';
////        else
////          return null;
//      },
      onSaved: onSaved,
      obscureText: obscureText,

    );
  }
}
