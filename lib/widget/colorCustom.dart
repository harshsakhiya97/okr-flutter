import 'dart:ui';

class customColor{
  final barColor = const Color(0xFF000e21);
  final bkColor = const Color(0xFF51713a);
  final bColor = const Color(0xFFD6C4DF);
  final dgrColor = const Color(0xFF878787);
  final grColor = const Color(0xFFAFAFAF);
  final greyColor = const Color(0xFFF6F6F6);
 final detailColor = const Color(0xFFAF7EB9);
 final metColor = const Color(0xFF565656);
  final logoColor = const Color(0xFFFEA201);
 //#FEA201
}

