import 'package:flutter/material.dart';


class MyNavigator {

  static void goToIntro(BuildContext context) {
    Navigator.pushNamed(context, "/intro");
  }

  static void goToLogin(BuildContext context) {
    Navigator.pushNamed(context, "/login");
  }

  static void goToSignup(BuildContext context) {
    Navigator.pushNamed(context, "/signup");
  }

  static void goToForget(BuildContext context) {
    Navigator.pushNamed(context, "/forget");
  }

  static void goToBottom(BuildContext context) {
    Navigator.pushNamed(context, "/bottom");
  }

  static void goToHome(BuildContext context) {
    Navigator.pushNamed(context, "/home");
  }

  static void goToSearch(BuildContext context) {
    Navigator.pushNamed(context, "/search");
  }

  static void goToFilter(BuildContext context) {
    Navigator.pushNamed(context, "/filter");
  }

   static void goToBookmark(BuildContext context) {
    Navigator.pushNamed(context, "/bookmark");
  }

  static void goToBookmarkHome(BuildContext context) {
    Navigator.pushNamed(context, "/bookmarkhome");
  }

  static void goToCall(BuildContext context) {
    Navigator.pushNamed(context, "/call");
  }

  static void goToDetail(BuildContext context) {
    Navigator.pushNamed(context, "/test");
  }

  static void goToAppDrawer(BuildContext context) {
    Navigator.pushNamed(context, "/appdrawer");
  }

  static void goToMyProfile(BuildContext context) {
    Navigator.pushNamed(context, "/myprofile");
  }

  static void goToChangePassword(BuildContext context) {
    Navigator.pushNamed(context, "/changepassword");
  }

  static void goToNotificationScreen(BuildContext context) {
    Navigator.pushNamed(context, "/notification");
  }

  static void goToAboutUs(BuildContext context) {
    Navigator.pushNamed(context, "/aboutus");
  }

  static void goToVerifyOtp(BuildContext context) {
    Navigator.pushNamed(context, "/verify");
  }

  static void goToSearchBar(BuildContext context) {
    Navigator.pushNamed(context, "/searchbar");
  }

  static void goToAllTestScreen(BuildContext context) {
    Navigator.pushNamed(context, "/alltest");
  }

  static void goToOncoScreen(BuildContext context) {
    Navigator.pushNamed(context, "/onco");
  }

}
