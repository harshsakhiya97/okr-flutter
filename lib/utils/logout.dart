import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/pages/login_screen.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogoutOverlay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LogoutOverlayState();
}

class LogoutOverlayState extends State<LogoutOverlay>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  final barColor = const Color(0xFF59a23b);
//  final bkColor = const Color(0xFFBE9FCB);
  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
              margin: EdgeInsets.all(20.0),
              padding: EdgeInsets.all(15.0),
              height: 180.0,

              decoration: BoxDecoration(
                  color: customColor().logoColor,
                  // gradient: LinearGradient(colors: [barColor, bkColor],
                  //   begin: Alignment.centerLeft,
                  //   end: Alignment.centerRight,
                  // ),
                  border: Border.all(
                    color: Colors.grey,
                    style: BorderStyle.solid,
                  ),

                  borderRadius: BorderRadius.circular(15.0)),
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20.0, right: 20.0),
                        child: Text(
                          "Are you sure, you want to logout?",
                          style: TextStyle(color: Colors.white, fontSize: 16.0, fontFamily: 'Montserrat',),
                        ),
                      )),
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: ButtonTheme(
                                height: 35.0,
                                minWidth: 110.0,
                                child: RaisedButton(
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0)),
                                  splashColor: Colors.white.withAlpha(40),
                                  child: Text(
                                    'Logout',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat',
                                        fontSize: 13.0),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      logout();

                                    });
                                  },
                                )),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
                              child:  ButtonTheme(
                                  height: 35.0,
                                  minWidth: 110.0,
                                  child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5.0)),
                                    splashColor: Colors.white.withAlpha(40),
                                    child: Text(
                                      'Cancel',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Montserrat',
                                          fontSize: 13.0),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        Navigator.of(context).pop();
                                        /* Route route = MaterialPageRoute(
                                          builder: (context) => LoginScreen());
                                      Navigator.pushReplacement(context, route);
                                   */ });
                                    },
                                  ))
                          ),
                        ],
                      ))
                ],
              )),
        ),
      ),
    );
  }

  void logout() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    SharedPref().remove("user");
    sharedPreferences.clear();
    sharedPreferences.setInt("intro", 1);
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
          (Route<dynamic> route) => false,
    );
  }

// update() async {
//   String  tokenId ;
//   User userTemp = new User();
//   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//   String token = sharedPreferences.getString("token");
//   int userId = sharedPreferences.getInt("id");
//   userTemp.setId(userId);
//   if (tokenId == null) {
//     userTemp.setFireBaseId(null);
//   } else {
//     userTemp.setFireBaseId(tokenId);
//   }
//   var body = json.encode(userTemp);
//   String call;
//   var response = await http.post(AppConstants.Firebase_URL,
//       headers: {
//         HttpHeaders.contentTypeHeader: "application/json",
//         HttpHeaders.authorizationHeader: "Bearer $token"
//       },
//       body: body);
//
//   if (response.statusCode == 200) {
//     bool flag = false;
//     String s = response.body;
//     if (s != null) {
//       if (s.trim() == ("SUCCESS")) {
//         flag = true;
//       }
//     }
//   }
// }


// void getUserLog() async{
//   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//   String token = sharedPreferences.getString("token");
//   User userResp = new User();
//   int userId = sharedPreferences.getInt("id");
//   userResp.setId(userId);
//   UserLogs userLogs = new UserLogs(user: userResp, predefinedName: "logout");
//   var body = json.encode(userLogs);
//   var jsonResponse;
//   try{
//   var response = await http.post(AppConstants.log_URL,
//       headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
//       body: body);
//   if (response.statusCode == 200) {
//     jsonResponse = json.decode(response.body);
//     if(jsonResponse != null) {
//      // Get.to(BottomHome());
//       update();
//       logout();
//     }
//     else{
//       ShowToast().showShortToast("Server Problem");
//
//     }
//   }
//   else{
//     ShowToast().showShortToast("Server Problem");
//   }
//   }on Exception catch (_){
//     ShowToast().showShortToast("Check Your Internet Connection");
//   }
// }
}