class ChatModel {
  final String name;
  final String message;


  ChatModel({this.name, this.message });
}

List<ChatModel> dummyData = [
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic Panel Ploidy Analysis + FISH-B-ALL, Molecular: IKZF1 mutation analysis, FISH for Ph1 Like ALL",
//      message: "Rs. 35,000"),
//
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic panel Ploidy Analysis + FISH - B-ALL, Molecular: IKZF1 mutation analysis Reflex to FISH for Ph1 Like ALL.",
//      message: "Rs. 10,000"),
//
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic panel Ploidy Analysis + FISH - B-ALL, Molecular: IKZF1 mutation analysis Reflex to FISH for Ph1 Like ALL.",
//      message: "Rs. 10,000"),
//
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic panel Ploidy Analysis + FISH - B-ALL, Molecular: IKZF1 mutation analysis Reflex to FISH for Ph1 Like ALL.",
//      message: "Rs. 10,000"),
//
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic panel B-ALL Reflex to Ph1 Like ALL",
//      message: "Rs. 25,000"),
//
//  new ChatModel(
//      name: "OncoInsights™ Cytogenetic panel Ploidy Analysis + FISH - B-ALL, Molecular: IKZF1 mutation analysis Reflex to FISH for Ph1 Like ALL.",
//      message: "Rs. 10,000"),


];
