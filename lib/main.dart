import 'package:flutter/material.dart';
import 'package:okr/home/advance_search.dart';
import 'package:okr/home/favouraite_screen.dart';
import 'package:okr/home/profile.dart';
import 'package:okr/home/save_search.dart';
import 'package:okr/home/search_result.dart';
import 'package:okr/navigation/my_profile.dart';
import 'package:okr/pages/login_screen.dart';
import 'package:okr/pages/splash_screen.dart';
import 'package:okr/pages/verify_otp.dart';
import 'package:okr/widget/app_drawer.dart';

import 'home/home_screen.dart';
import 'navigation/about_us.dart';
import 'navigation/change_password.dart';
import 'pages/forget_screen.dart';
import 'pages/signup_screen.dart';

var routes = <String, WidgetBuilder>{
  // "/intro": (BuildContext context) => IntroScreen(),
  "/login": (BuildContext context) => LoginScreen(),
  "/signup": (BuildContext context) => SignupScreen(),
  "/forget": (BuildContext context) => ForgetScreen(),
  // "/bottom": (BuildContext context) => BottomHome(),
  "/home": (BuildContext context) => HomeScreen(),
   "/search": (BuildContext context) => SaveSearch(),
   "/filter": (BuildContext context) => AdvanceSearch(),
   "/bookmark": (BuildContext context) => Favouraite(),
  // "/call": (BuildContext context) => CallScreen(),
  // "/test": (BuildContext context) => Detail(),
  "/appdrawer": (BuildContext context) => AppDrawer(),
  "/changepassword": (BuildContext context) => ChangePassword(),
   "/myprofile": (BuildContext context) => MyProfile(),
  "/aboutus": (BuildContext context) => AboutUs(),
  // "/notification": (BuildContext context) => NotificationScreen(),
  "/verify": (BuildContext context) => VerifyOtp(),
   "/alltest": (BuildContext context) => SearchResult(),
  // "/onco": (BuildContext context) => OncoScreen(),
  // "/bookmarkhome": (BuildContext context) => BookMarkHomeScreen(),

};


void main() {
  // setupLocator();
  // Crashlytics.instance.enableInDevMode = true;
  // Pass all uncaught errors from the framework to Crashlytics.
  //FlutterError.onError = Crashlytics.instance.recordFlutterError;
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
     // navigatorKey: Get.key,
      theme: new ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: SplashScreen(),
      routes: routes)
  );
}


