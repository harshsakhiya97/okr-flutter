import 'dart:convert';
import 'dart:io';

import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/home/home_screen.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/UserDto.dart';
import 'package:okr/services/models/UserLog.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/home/bottom_home.dart';
// import 'package:splash/home/home_screen.dart';
// import 'package:splash/services/appconstant.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/UserDto.dart';
// import 'package:splash/services/models/UserLog.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/utils/my_navigator.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:splash/widget/colorCustom.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return LoginScreenState();
  }
}
  class LoginScreenState extends State<LoginScreen>{
 bool checkboxvalue = false;

 final scaffoldKey = new GlobalKey<ScaffoldState>();
 final formKey = new GlobalKey<FormState>();
 bool autoValidate = false;
 final TextEditingController emailController = new TextEditingController();
 final TextEditingController passwordController = new TextEditingController();
 String logemail;
 String logpassword;
 void submit() {
   final form = formKey.currentState;

   if (form.validate()) {
     form.save();
     signIn(emailController.text, passwordController.text);
   }
   else {
//    If all data are not valid then start auto validation.
     setState(() {
       autoValidate = true;
     });
   }
 }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(

      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(

                      padding: EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
                      child: Center(
                        child: Text(
                            'SIGN IN',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Container(

                  margin: EdgeInsets.only(top: 60,left: 25,right: 25,bottom: 40),
                  child: Form(

                   key: formKey,
                    autovalidate: autoValidate,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          color: Colors.transparent,
                            child: Column(

                                children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 0.0,),
                                  child:  AMTextField(text: "Mobile No",
                                  readOnly: false,
                                  keyboardType:  TextInputType.phone,
                                    controller: emailController,
                                    validator: (value){


                                    if (value == null)
                                      return 'Enter Valid Mobile No';
                                    else
                                      return null;
                                  },
                                   // onSaved: (val) => logemail = val,
                                    obscureText: false,
                                  ) ,
                              ),
                                ],
                            ),

                        ),
                        SizedBox(height: 20.0),
                        Container(
                          color: Colors.transparent,
                         child: Padding(
                            padding: const EdgeInsets.only(left: 0,right: 0),
                              child: AMTextField(text: "Password",
                                readOnly: false,
                                controller: passwordController,
                                validator: (val) {
                                   if( val == null ) {
                                     return 'Enter Proper Password';
                                   }
                                    else
                                    return null;
                                       },
                                //    onSaved: (val) => logpassword = val,
                                    obscureText: true,
                              ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            //  SizedBox(height: 20.0,),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: CustomButton(
                    onPresed: (){
                      FocusScope.of(context).unfocus();
                     // signIn(emailController.text, passwordController.text);
                      submit();
                    },
                    text: "Sign In",
                ),
              ),
              SizedBox(height: 5.0),
              Container(
                alignment: Alignment(1.0, 0.0),
                padding: EdgeInsets.only(top:5.0, right: 40.0),
                child: InkWell(
                  onTap: (){
                    MyNavigator.goToForget(context);
                  },
                  child: Text(
                    'Forgot Password?',
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                        fontWeight: FontWeight.w600,
                     fontFamily: 'Montserrat',
                        decoration: TextDecoration.none),
                  ),
                ),
              ),
              SizedBox(height: 60.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Don't have an account?",
                    style: TextStyle(
                      fontFamily: 'Montserrat',

                      fontSize: 14,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(width: 5.0,),
                  InkWell(
                    onTap: (){
                      MyNavigator.goToSignup(context);
                    },
                    child: Text("SIGN UP",
                    style: TextStyle(
                      color: customColor().barColor,
                      fontSize: 14,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                    ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),

    );
  }

 signIn(String email, pass) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   UserDto userDto = new UserDto(email,pass);

   var body = json.encode(userDto);

   var jsonResponse;
   try {
     var url = Uri.parse(AppConstants.LOGIN_URL);
     var response = await http.post(url,
         headers: {"Content-Type": "application/json"},
         body: body);
     if (response.statusCode == 200) {
       jsonResponse = json.decode(response.body);
       if (jsonResponse != null) {
         sharedPreferences.setString("token", jsonResponse['token']);

         getuserDetail(userDto);

       }
       else {
         ShowToast().showToast(context, "Something Went Wrong");
       }
     }
     else {
       ShowToast().showToast(context, "Check Your Username and Password");
     }
   }on Exception catch (_){
     ShowToast().showToast(context, "Check Your Internet Connection");
   }
 }
 void getuserDetail(UserDto userDto) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   User user = User(username: userDto.username);
   var body = json.encode(user);
   var jsonResponse;
   try {
     var url = Uri.parse(AppConstants.UserDetail_URL);
     var response = await http.post(url,
         headers: {"Content-Type": "application/json"},
         body: body);
     if (response.statusCode == 200) {
       jsonResponse = json.decode(response.body);
       if (jsonResponse['isDeactive'] == 'Y') {
         ShowToast().showToast(context, "Your account has been deactivate");
       }
       else if (jsonResponse != null) {
         SharedPref().save("user", jsonResponse);

         int userId = (jsonResponse['id']);
         // String userName = (jsonResponse['username']);
         // String userFirstName = (jsonResponse['fullName']);
         // String phone = (jsonResponse['phone']);
         // String email = (jsonResponse['email']);
         // user.setUsername(userName);
         // user.setEmail(email);
         // user.setFirstName(userFirstName);
         // user.setPhone(phone);
         // sharedPreferences.setInt("id", user.getId());
         // sharedPreferences.setInt("sync", 0);
         // sharedPreferences.setString("userEmail", user.getEmail());
         // sharedPreferences.setString("firstName", user.getFirstName());
         // sharedPreferences.setString("phone", user.getPhone());
         // sharedPreferences.setString("username", user.getUsername());
         getUserLog(userId);
         // MyNavigator.goToHome(context);
       }
     }
     else {
       ShowToast().showToast(context, "Server Problem");
     }
   } on Exception catch (_) {
     ShowToast().showToast(context, "Check Your Internet Connection");
   }
 }
 loadSharedPrefs() async {
   try {
     User user = User.fromJson(await SharedPref().read("user"));
     setState(() {
       print(user);
     });
   } catch (Excepetion) {
     // do something
   }
 }

 void getUserLog(int id) async{
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   String token = sharedPreferences.getString("token");
   User userResp = new User(id: id);
    UserLogs userLogs = new UserLogs(user: userResp);
   var body = json.encode(userLogs);
   var jsonResponse;
   var url = Uri.parse(AppConstants.log_URL);
   var response = await http.post(url,
       headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
       body: body);
   if (response.statusCode == 200) {
     jsonResponse = json.decode(response.body);
     if(jsonResponse != null) {
       ShowToast().showToast(context, "Successfull");
       Navigator.pushAndRemoveUntil(
         context,
         MaterialPageRoute(builder: (context) => HomeScreen()),
             (Route<dynamic> route) => false,
       );
     }
     else{
       ShowToast().showToast(context, "Server Problem");

     }
   }
   else{
     ShowToast().showToast(context, "Server Problem");
   }
 }


  }

