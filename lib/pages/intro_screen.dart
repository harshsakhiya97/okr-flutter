// import 'dart:io';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/widget/colorCustom.dart';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:splash/Data/walkthrough.dart';
// import 'package:splash/utils/my_navigator.dart';
// import 'package:splash/widget/custom_button.dart';
//
// class IntroScreen extends StatefulWidget {
//
//   @override
//   IntroScreenState createState() {
//     return IntroScreenState();
//   }
// }
//
// class IntroScreenState extends State<IntroScreen> {
//
//   List<SliderModel> slides = new List<SliderModel>();
//   int currentIndex = 0;
//   PageController pageController = new PageController(initialPage: 0);
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     slides = getSlides();
//   }
//
//   Widget pageIndexIndicator(bool isCurrentPage)
//   {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 4.0),
//       height: isCurrentPage ? 8.0 : 6.0,
//       width: isCurrentPage ? 30.0 : 30.0,
//       decoration: BoxDecoration(
//       color: isCurrentPage ? customColor().barColor : Colors.grey,
//           borderRadius: BorderRadius.circular(12)
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Column(
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.only(top: 50.0,bottom: 50.0),
//             child: Image(
//               image: AssetImage('images/zzz.PNG',),
//               height: 50,
//               width: 211,
//             ),
//           ),
//           Expanded(
//             child: Container(
//               child: Center(
//                 child: PageView.builder(
//                   controller: pageController,
//                     itemCount: slides.length,
//                     onPageChanged: (val){
//                       setState(() {
//                         currentIndex = val;
//                       });
//                     },
//                     itemBuilder: (context, index){
//                       return SliderTile(
//                     //    imageLogo: slides[index].getImageLogo(),
//                         imagePath: slides[index].getImagePath(),
//                         title: slides[index].getTitle(),
//                         desc: slides[index].getDesc(),
//                       );
//                     }),
//               ),
//             ),
//           ),
//         ],
//       ),
//       bottomSheet:
//       currentIndex != slides.length - 1 ? Container(
//           width: MediaQuery.of(context).size.width,
//           height: Platform.isIOS ? 70 : 60,
//           color: Colors.white,
//         padding: EdgeInsets.symmetric(horizontal: 20.0),
//         child: Row(
//
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             GestureDetector(
//               onTap: (){
//                 pageController.animateToPage(slides.length - 1, duration: Duration(milliseconds: 200), curve: Curves.linear);
//               },
//               child: Text("SKIP"),
//             ),
//             Row(
//                 children: <Widget>[
//                   for(int i = 0; i < slides.length; i++) currentIndex == i ? pageIndexIndicator(true) : pageIndexIndicator(false)
//                 ],
//             ),
//             GestureDetector(
//               onTap: (){
//                 pageController.animateToPage(currentIndex + 1, duration: Duration(milliseconds: 200), curve: Curves.linear);
//               },
//               child: Text("NEXT"),
//             ),
//           ],
//         ),
//       ) : Container(
//         alignment: Alignment.center,
//         width: MediaQuery.of(context).size.width,
//         height: Platform.isIOS ? 150 : 140,
//         color: Colors.white,
//        padding: EdgeInsets.symmetric(horizontal: 40.0,),
//       child: CustomButton(onPresed: (){
//         login();
//       },
//           text: "Get Started",
//       ),
// //      child: FlatButton(
// //
// //          onPressed: null,
// //          child: Image(
// //            image: AssetImage('images/Getstarted.PNG'),
// //            height: 60,
// //          width: 300,),
// //      ),
//       ),
//     );
//   }
//
//   void login() async{
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     sharedPreferences.setInt("intro", 1);
//     MyNavigator.goToLogin(context);
//
//   }
// }
//
//  class SliderTile extends StatelessWidget {
//   String imageLogo, imagePath, title, desc;
//   SliderTile({this.imageLogo, this.imagePath, this.title, this.desc});
//    @override
//    Widget build(BuildContext context) {
//      return Container(
//        child: Column(
//          children: <Widget>[
//            Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//
//                 Container(
//                     height: 220.0,width: 200.0,
//                     child: SvgPicture.asset(imagePath,color: customColor().barColor,),),
//                 SizedBox(height: 20,),
//                Container(
//                  child: Text(title, style: TextStyle(
//                  fontSize: 24,
//                    fontFamily: 'Montserrat',
//                    fontWeight: FontWeight.bold,
//                  ),
//                  ),
//                ),
//                SizedBox(height: 12,),
//                Container(
//                  child: Padding(
//                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
//                    child: Text(desc, textAlign: TextAlign.center,style: TextStyle(
//                     fontSize: 18,
//                        fontFamily: 'Montserrat',
//                        fontWeight: FontWeight.w400,
//                        color: customColor().dgrColor,
//
//                    ),
//                    ),
//                  ),
//                ),
//              ],
//            )
//          ],
//        ),
//      );
//    }
//  }
//
