import 'dart:convert';
// import 'package:get/get.dart';
import 'package:flutter/services.dart';
// import 'package:http/http.dart' as http;
// import 'package:splash/home/bottom_home.dart';
// import 'package:splash/pages/password_change.dart';
// import 'package:splash/services/appconstant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/UserDto.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/utils/my_navigator.dart';
// import 'package:splash/widget/colorCustom.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/services/share_pref.dart';

// ignore: must_be_immutable
class ForgetVerifyOtp extends StatelessWidget {
  //final User user;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  bool autoValidate = false;
  String otp;
  final TextEditingController txtotp = new TextEditingController();
  final barColor = const Color(0xFF815F8F);


   //ForgetVerifyOtp({Key key, this.user}) : super(key: key);
  void submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
     // verifyotp();
      //  MyNavigator.goToLogin(context);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  SingleChildScrollView(
        child: Column(

          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(

                    padding: EdgeInsets.fromLTRB(10.0, 120.0, 10.0, 0.0),
                    child: Center(

                      child: Text(
                        'VERIFY OTP',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 35.0,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Form(
              key: formKey,
              autovalidate: autoValidate,
              child: Container(
                padding: EdgeInsets.only(top: 80,left: 25,right: 25,bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(

                      color: Colors.transparent,
                      child: Column(

                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 0.0,),
                            child: AMTextField(text: "Enter Otp",
                              controller: txtotp,validator: (val) {
                                if (val.length < 3) {
                                  return 'Otp is incorrect';
                                }
                                else
                                  return null;
                              },

                              obscureText: false,
                            ),
//
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Container(
                      alignment: Alignment(1.0, 0.0),
                      padding: EdgeInsets.only(top:5.0, right: 40.0),
                      child: InkWell(
                        onTap: (){

                      // sendingOtp(user);
                        },
                        child: Text(
                          'Resend OTP',
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey,
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Montserrat',
                              decoration: TextDecoration.none),
                        ),
                      ),
                    ),
                    SizedBox(height: 30.0),
                    CustomButton(onPresed: (){
                      submit();
                    }, text: 'Verify OTP',),

                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

//   void verifyotp() {
//     String actualOtp = user.getOtp();
//     if(actualOtp == txtotp.text)
//     {
//      getuserDetail();
//     }
//     else
//     {
//       ShowToast().showShortToast("Invalid Otp");
//     }
//   }
//
//   void sendingOtp(User user)async {
//
//     var body = json.encode(user);
//     String call;
//     var response = await http.post(AppConstants.Verify_URL,
//         headers: {"Content-Type": "application/json"},
//         body: body);
//     if (response.statusCode == 200) {
//       call = response.body;
//       otp = response.body;
//       user.setOtp(otp);
//     }
//   }
//
//   void getuserDetail() async{
//     var body = json.encode(user);
//     var jsonResponse;
//
//     var response = await http.post(AppConstants.UserDetail_URL,
//         headers: {"Content-Type": "application/json"},
//         body: body);
//     if (response.statusCode == 200) {
//       jsonResponse = json.decode(response.body);
//       if (jsonResponse != null)
//       {
//         int userId = (jsonResponse['id']);
//         user.setId(userId);
//         sendId(user);
//       }
//     }
//    else{
//      print("HelloAtil");
//     }
//   }
//
//    sendId(User user) async {
//     final result = await Get.to(PasswordChange(user : user));
// //    final result = await Navigator.push(
// //        context,
// //        MaterialPageRoute(
// //            builder: (context) => PasswordChange(
// //              userID: userID,
// //            )));
//     print(result);
//   }



}




