import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
// import 'package:http/http.dart' as http;
// import 'package:splash/pages/verifyotp_screen.dart';
// import 'package:splash/services/appconstant.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/utils/my_navigator.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:okr/pages/password_change.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/UserDto.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'otp_screens.dart';
// import 'package:splash/widget/colorCustom.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';

class ForgetScreen extends StatefulWidget {
  @override
  ForgetScreenState createState() {
    return ForgetScreenState();
  }
}
class ForgetScreenState extends State<ForgetScreen> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  bool autoValidate = false, isSend = false;
  String otps, email, name, phone, username;
  int userId;
  User userLoad = User();
  final TextEditingController txtemail = new TextEditingController();
  final barColor = const Color(0xFF815F8F);
  void submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
    //  userDetail(txtemail.text);

    }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        autoValidate = true;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(

                      padding: EdgeInsets.fromLTRB(10.0, 120.0, 10.0, 0.0),
                      child: Center(

                        child: Text(
                          'FORGOT PASSWORD',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 30.0,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
                Container(
                  padding: EdgeInsets.only(top: 20.0,left: 44,right: 38),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                              "We just need your registered email to send you password reset instructions",textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 13,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w400,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                ),
              Form(
                key: formKey,
                autovalidate: autoValidate,
                child: Container(
                  padding: EdgeInsets.only(top: 30,left: 25,right: 25,bottom: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(

                        color: Colors.transparent,
                        child: Column(

                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 0.0,),
                                child: AMTextField(text: "Email",
                                  keyboardType: TextInputType.emailAddress,
                                controller: txtemail,
                                readOnly: false,
                                validator: (value){
                                  Pattern pattern =
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regex = new RegExp(pattern);
                                  if (!regex.hasMatch(value))
                                    return 'Enter Valid Email';
                                  else
                                    return null;
                                },
                              //  onSaved: (val) => fogemail = val,
                                  obscureText: false,
                                ),
//
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                      CustomButton(onPresed: (){
                        submit();
                      }, text: 'Reset Password',),
                      SizedBox(height: 200.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Don't have an account?",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 14,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(width: 5.0,),
                          InkWell(
                            onTap: (){
                              MyNavigator.goToSignup(context);
                            },
                            child: Text("SIGN UP",
                              style: TextStyle(
                                color: customColor().barColor,
                                fontSize: 14,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
    );
  }
  sendingOtp()  async{
    var body = json.encode(userLoad);
    try {
      String call;
      var url = Uri.parse(AppConstants.Verify_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        call = response.body;
        otps = response.body;
        if(otps != null) {
          userLoad.otp = otps;
       //   ShowToast().showToast(context, otps);
          if (isSend)
          {
            isSend = false;
            sendOtp();
          }
        }
      }
    }on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }

  userDetail(String username) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    User user = User(username: username);
    var body = json.encode(user);
    var jsonResponse;
    try {
      var url = Uri.parse(AppConstants.UserDetail_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        if (jsonResponse['isDeactive'] == 'Y') {
          ShowToast().showToast(context, "Your account has been deactivate");
        }
        else if (jsonResponse != null) {
          userLoad.username = (jsonResponse['username']);
          userLoad.id = (jsonResponse['id']);
          userLoad.firstName = (jsonResponse['fullName']);
          userLoad.phone = (jsonResponse['phone']);
          userLoad.email = (jsonResponse['email']);

          ShowToast().showToast(context, "Successfull");
          sendingOtp();
        }
      }
      else {
        ShowToast().showToast(context, "Server Problem");
      }
    } on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }

  sendOtp() async {

    final result = await  Navigator.push(context, MaterialPageRoute(
        builder: (context) => OtpScreens(otpLength: 4, validateOtp: validateOtp, routeCallback: moveToNextScreen,resend: sendingOtp,)
    ));
  }
  void moveToNextScreen(context) {
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => PasswordChange(
          user: userLoad,
        )));
  }

  Future<String> validateOtp(String otpCheck) async {
    await Future.delayed(Duration(milliseconds: 2000));
    if (otps.isNotEmpty && otpCheck == otps) {
      return null;
    } else {
      return "The entered Otp is wrong";
    }
  }

}