import 'dart:convert';
import 'dart:io';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/pages/verify_otp.dart';
// import 'package:splash/pages/verifyotp_screen.dart';
// import 'package:splash/services/appconstant.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/UserDto.dart';
// import 'package:splash/services/share_pref.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/utils/my_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:splash/widget/colorCustom.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';
import 'package:flutter/services.dart';
import 'package:okr/home/home_screen.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/UserDto.dart';
import 'package:okr/services/models/UserLog.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'otp_screens.dart';

class SignupScreen extends StatefulWidget {
  @override
  SignupScreenState createState() {
    return SignupScreenState();
  }
}

class SignupScreenState extends State<SignupScreen> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
//  String signemail;
//  String signpassword;
//  String signphone;
//  String signconfpass;
//  String signname;
  bool autoValidate = false, isSend = false;
  User userLoad = User();
  String otps;
  final TextEditingController pass = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController name = TextEditingController();
  final TextEditingController lname = TextEditingController();
  final TextEditingController companyname = TextEditingController();


  // User userMaster = User();

  void submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      // Home Page
     verify(email.text, phone.text, name.text, pass.text, lname.text, companyname.text);
    }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        autoValidate = true;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(

                    padding: EdgeInsets.only(top: 80,),
                    child: Center(
                      child: Text(
                        'SIGN UP',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 30.0,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
      ],
    ),
    ),

                  Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: Container(
                      padding: EdgeInsets.only(top: 30,left: 25,right: 25,bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                                child: AMTextField(text: "First Name",
                                        controller: name,
                                        readOnly: false,
                                        validator: (value) {
                                   if (value.length < 3)
                                    return 'Name must be more than 2 charater';
                                  else
                                    return null;
                                },
                              //  onSaved: (val) => signname = val,
                                  obscureText: false,
                                ),
                            ),
                          ),
                          SizedBox(height: 15.0),
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                              child: AMTextField(text: "Last Name",
                                controller: lname,
                                readOnly: false,
                                validator: (value) {
                                  if (value.length < 3)
                                    return 'Name must be more than 2 charater';
                                  else
                                    return null;
                                },
                                //  onSaved: (val) => signname = val,
                                obscureText: false,
                              ),
                            ),
                          ),
                          SizedBox(height: 15.0),
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                                  child: AMTextField(text: "Mobile No.",
                                    keyboardType:  TextInputType.phone,
                                controller: phone,
                                readOnly: false,
                                validator: (value) {
                                  if (value.length != 10)
                                  return 'Mobile Number must be of 10 digit';
                                  else
                                    return null;
                                 },
                            //    onSaved: (val) => signphone = val,
                                    obscureText: false,
                                  ),
                            ),
                          ),
                          SizedBox(height: 15.0,),
                          Container(

                            color: Colors.transparent,
                            child: Column(

                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 0.0,)  ,
                                    child: AMTextField(text: "Email",
                                      keyboardType: TextInputType.emailAddress,
                                      controller: email,
                                      readOnly: false,
                                      validator: (value){
                                        Pattern pattern =
                                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                        RegExp regex = new RegExp(pattern);
                                        if (!regex.hasMatch(value))
                                          return 'Enter Valid Email';
                                        else
                                          return null;
                                      },
                              //        onSaved: (val) => signemail = val,
                                      obscureText: false,
                                    ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 15.0),
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                              child: AMTextField(text: "Company Name.",
                                keyboardType:  TextInputType.name,
                                controller: companyname,
                                readOnly: false,
                                validator: (value) {
                                  if (value.length < 1)
                                    return 'Select Company Name';
                                  else
                                    return null;
                                },
                                //    onSaved: (val) => signphone = val,
                                obscureText: false,
                              ),
                            ),
                          ),
                          SizedBox(height: 15.0),
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                                  child: AMTextField(text: "Password",
                                    controller: pass,
                                readOnly: false,
                                validator: (val) {
                        if( val.length < 6 ) {
                                   return 'Password too short';
                                     }
                                    if( val.length >= 15 ) {
                                         return 'Password too big';
                                     }
                                         else
                                  return null;
                                         },
                              //  onSaved: (val) => signpassword = val,
                                obscureText: true,
                                  ),
                            ),
                          ),
                          SizedBox(height: 15.0,),
                          Container(
                            // height: Platform.isIOS ? 62 : 56,
                            // height: 56.0,
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 0,right: 0),
                                child: AMTextField(text: "Retype Password",
                                  controller: confirmPass,
                                readOnly: false,
                                validator: (val)
                                {
                                  if(val != pass.text)
                                    return 'Not Match';
                                  else
                                  return null;
                                },
                             //   val.length < 4 ? 'Password too short' : null,
                             //   onSaved: (val) => signconfpass = val,
                                obscureText: true,
                                ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                       CustomButton(
                           onPresed: (){
                             FocusScope.of(context).unfocus();
                             submit();
                           },
                           text: "Sign Up",
                       ),
                          SizedBox(height: 20.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Have an account?",
                                style: TextStyle(

                                  fontFamily: 'Montserrat',
                                  fontSize: 14,
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(width: 5.0,),
                              InkWell(
                                onTap: (){
                                  MyNavigator.goToLogin(context);
                                },
                                child: Text("SIGN IN",
                                  style: TextStyle(
                                    color: customColor().barColor,
                                    fontSize: 14,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }


  verify(String email, String phone, String name, String pass, String lastname, String company,) async {
    User user = new User(email: email, phone: phone, firstName: name, currentPassword: pass, lastName: lastname, companyName: company);
    var body = json.encode(user);
    try {
      String duplicateError;
      var url = Uri.parse(AppConstants.Check_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        duplicateError = response.body;
        if (duplicateError == "NO-DUP") {
          userLoad = user;
          isSend = true;
          sendingOtp();
          ShowToast().showToast(context,
              "Otp has been send to register Email");
        }
        else if (duplicateError == "EMAIL-DUP") {
          ShowToast().showToast(context,"Email is already registered");
        }
        else if (duplicateError == "PHONE-DUP") {
          ShowToast().showToast(context,"Mobile number is already registered");
        }
        else if (duplicateError == "EMAIL-PHONE-DUP") {
          ShowToast().showToast(context,"Same Number and Same Email Found");
        }
      }
      else {
        ShowToast().showToast(context,"Server Problem");
      }
    }on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }

  }

  sendingOtp()  async{
    var body = json.encode(userLoad);
    try {
      String call;
      var url = Uri.parse(AppConstants.Verify_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        call = response.body;
        otps = response.body;
        if(otps != null) {
          userLoad.otp = otps;
        //  ShowToast().showToast(context, otps);
          if (isSend)
          {
            isSend = false;
            sendOtp();
          }
        }
      }
    }on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }
  //
  //
  sendOtp() async {

    final result = await  Navigator.push(context, MaterialPageRoute(
        builder: (context) => OtpScreens(otpLength: 4, validateOtp: validateOtp, routeCallback: userRegister,resend: sendingOtp,)
    ));
  }
  void userRegister(context) async {
    var body = json.encode(userLoad);
    var jsonResponse;
    try{
      var response = await http.post(Uri.parse(AppConstants.Register_URL),
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        if(jsonResponse != null) {
          String userName = (jsonResponse['username']);
          userLoad.username = userName;
          // // int userId = (jsonResponse['id']);
          //  String userFirstName = (jsonResponse['fullName']);
          //  String phone = (jsonResponse['phone']);
          //  String email = (jsonResponse['email']);
          //  User user = User(username: userName);
          // user.setId(userId);
          // user.setUsername(userName);
          // user.setEmail(email);
          // user.setFirstName(userFirstName);
          // user.setPhone(phone);

          // sharedPref.save("user",jsonResponse);
          getAuth(userLoad);
        }else{
          ShowToast().showToast(context, "Something went wrong");
        }
      }
    }on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }
  //
  //  sendingOtp(User user) async {
  //
  //    var body = json.encode(user);
  //    String call;
  //    var response = await http.post(AppConstants.Verify_URL,
  //        headers: {"Content-Type": "application/json"},
  //        body: body);
  //    if (response.statusCode == 200) {
  //      call = response.body;
  //      otp = response.body;
  //      user.setOtp(otp);
  //      sendOtp(user);
  //
  //    }
  //  }
  Future<String> validateOtp(String otpCheck) async {
    await Future.delayed(Duration(milliseconds: 2000));
    if (otps.isNotEmpty && otpCheck == otps) {
      return null;
    } else {
      return "The entered Otp is wrong";
    }
  }

  void getAuth(User userLoad) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    UserDto userDto = new UserDto(userLoad.username,userLoad.currentPassword);

    var body = json.encode(userDto);

    var jsonResponse;
    try {
      var url = Uri.parse(AppConstants.LOGIN_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          sharedPreferences.setString("token", jsonResponse['token']);

          getuserDetail(userDto);

        }
        else {
          ShowToast().showToast(context, "Something Went Wrong");
        }
      }
      else {
        ShowToast().showToast(context, "Check Your Username and Password");
      }
    }on Exception catch (_){
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }

  void getuserDetail(UserDto userDto) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    User user = User(username: userDto.username);
    var body = json.encode(user);
    var jsonResponse;
    try {
      var url = Uri.parse(AppConstants.UserDetail_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        if (jsonResponse['isDeactive'] == 'Y') {
          ShowToast().showToast(context, "Your account has been deactivate");
        }
        else if (jsonResponse != null) {
          SharedPref().save("user", jsonResponse);

          int userId = (jsonResponse['id']);
          // String userName = (jsonResponse['username']);
          // String userFirstName = (jsonResponse['fullName']);
          // String phone = (jsonResponse['phone']);
          // String email = (jsonResponse['email']);
          // user.setId(userId);
          // user.setUsername(userName);
          // user.setEmail(email);
          // user.setFirstName(userFirstName);
          // user.setPhone(phone);
          // sharedPreferences.setInt("id", user.getId());
          // sharedPreferences.setInt("sync", 0);
          // sharedPreferences.setString("userEmail", user.getEmail());
          // sharedPreferences.setString("firstName", user.getFirstName());
          // sharedPreferences.setString("phone", user.getPhone());
          // sharedPreferences.setString("username", user.getUsername());
          getUserLog(userId);
        //  MyNavigator.goToHome(context);
        }
      }
      else {
        ShowToast().showToast(context, "Server Problem");
      }
    } on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }

  void getUserLog(int id) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    User userResp = new User(id: id);
    UserLogs userLogs = new UserLogs(user: userResp);
    var body = json.encode(userLogs);
    var jsonResponse;
    var url = Uri.parse(AppConstants.log_URL);
    var response = await http.post(url,
        headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
        ShowToast().showToast(context, "Successfull");
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
              (Route<dynamic> route) => false,
        );
      }
      else{
        ShowToast().showToast(context, "Server Problem");

      }
    }
    else{
      ShowToast().showToast(context, "Server Problem");
    }
  }

}



