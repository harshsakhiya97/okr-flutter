import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/home/home_screen.dart';
import 'package:okr/pages/login_screen.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/share_pref.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//import 'package:splash/services/appconstant.dart';
//import 'package:splash/services/models/User.dart';
//import 'package:splash/services/models/UserLog.dart';
//import 'package:splash/services/show_toast.dart';
//import 'package:splash/utils/my_navigator.dart';
//import 'package:splash/widget/colorCustom.dart';
import 'package:flutter/services.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:http/http.dart' as http;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  int id, intro;
  User user;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 1), () => loadScreen());
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.white),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Center(
                          // child: Text(
                          //   'OKRS',
                          //   textAlign: TextAlign.center,
                          //   style: TextStyle(
                          //     fontFamily: 'Montserrat',
                          //     fontSize: 50.0,
                          //     letterSpacing: 2.0,
                          //     fontWeight: FontWeight.bold,
                          //   ),
                          // ),
                          child: Image(
                            image: AssetImage('images/logo.png',),
                            height: 74,
                            width: 250,
                          ),
                        ),
                      ),
                      // Container(
                      //   child: Center(
                      //     child: Text(
                      //       'Welcome to OKRS',
                      //       textAlign: TextAlign.center,
                      //       style: TextStyle(
                      //         fontFamily: 'Montserrat',
                      //         fontSize: 20.0,
                      //         fontWeight: FontWeight.normal,
                      //       ),
                      //     ),
                      //     // child: Image(
                      //     //   image: AssetImage('images/zzz.PNG',),
                      //     //   height: 50,
                      //     //   width: 211,
                      //     // ),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
    //           Padding(
    //             padding: const EdgeInsets.only(bottom: 50.0),
    //             child: Container(
    //               child: Column(
    //                 crossAxisAlignment: CrossAxisAlignment.center,
    //                 children: <Widget>[
    //                   Text("By", style: TextStyle(
    //                       fontWeight: FontWeight.bold,
    //                       fontSize: 14,
    //                       color: Colors.grey,
    //                       fontFamily: "Montserrat"
    //                   ),),
    //                 //  SizedBox(height: 10.0,),
    //                   Image.asset("images/1.png",height: 60.0,
    // width: 200.0,),
    //                 ],
    //               ),
    //             ),
    //           ),
            ],
          ),
        ],
      ),
    );
  }

  void loadScreen() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        if(user != null && user.id != null)
        {
          Timer(Duration(seconds: 1), () => Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
                (Route<dynamic> route) => false,
          ));
        }else{
          Timer(Duration(seconds: 1), () => Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => LoginScreen()),
                (Route<dynamic> route) => false,
          ));
        }
      });
    } catch (Excepetion) {
      Timer(Duration(seconds: 1), () => Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
            (Route<dynamic> route) => false,
      ));
      // do something
    }
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // id = sharedPreferences.getInt("id");
    // intro = sharedPreferences.getInt("intro");
    // if(id != null)
    //   {
    //     getUserLog(id);
    //     Timer(Duration(seconds: 1), () => MyNavigator.goToBottom(context));
    //   }
    // else if(intro != null)
    //   {
    //     Timer(Duration(seconds: 1), () => MyNavigator.goToLogin(context));
    //   }
    // else
    //   {
   //      Timer(Duration(seconds: 1), () => MyNavigator.goToLogin(context));
   // //   }
  }
  // void getUserLog(int id) async{
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   String token = sharedPreferences.getString("token");
  //   User userResp = new User();
  //   userResp.setId(id);
  //   UserLogs userLogs = new UserLogs(user: userResp, predefinedName: "login");
  //   var body = json.encode(userLogs);
  //   var jsonResponse;
  //
  //   var response = await http.post(AppConstants.log_URL,
  //       headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
  //       body: body);
  //   if (response.statusCode == 200) {
  //     jsonResponse = json.decode(response.body);
  //     if(jsonResponse != null) {
  //      // Timer(Duration(seconds: 1), () => MyNavigator.goToBottom(context));
  //     }
  //     else{
  //       ShowToast().showShortToast("Server Problem");
  //
  //     }
  //   }
  //   else{
  //     ShowToast().showShortToast("Server Problem");
  //   }
  // }
}