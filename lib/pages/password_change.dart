import 'dart:convert';
import 'package:flutter/services.dart';
// import 'package:get/get.dart';
// import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
// import 'package:splash/pages/login_screen.dart';
// import 'package:splash/services/appconstant.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/UserDto.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/widget/app_bar.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:okr/pages/password_change.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/UserDto.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';

class PasswordChange extends StatelessWidget {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  final User user;
  // UserDto userDto = UserDto();
  bool autoValidate = false;
  final TextEditingController pass = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();
  final barColor = const Color(0xFF815F8F);

  PasswordChange({Key key, this.user}) : super(key: key);
  void submit(BuildContext context) {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      changePassword(user, context);
    }
    else {
//    If all data are not valid then start auto validation.
//      setState(() {
//        autoValidate = true;
//      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(

                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      child: Center(
                        child: Image(
                          image: AssetImage('images/splash.png',),
                          height: 150,

                        ),
                        // child: Text(
                        //   'Change Password',
                        //   textAlign: TextAlign.center,
                        //   style: TextStyle(
                        //     fontFamily: 'Montserrat',
                        //     fontSize: 35.0,
                        //     fontWeight: FontWeight.bold,
                        //   ),
                        // ),
                      ),
                    ),
                  ],
                ),
              ),
              Form(
                key: formKey,
                autovalidate: autoValidate,
                child: Container(

                  padding: EdgeInsets.only(top: 60,left: 25,right: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        // height: Platform.isIOS ? 62 : 56,
                        // height: 56.0,
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0,right: 0),
                          child: AMTextField(text: "Password", obscureText: true,
                            controller: pass,
                            validator: (val) {
                              if( val.length < 5 ) {
                                return 'Password too short';
                              }
                              if( val.length >= 15 ) {
                                return 'Password too big';
                              }
                              else
                                return null;
                            },

                          ),

                        ),
                      ),
                      SizedBox(height: 15.0,),
                      Container(
                        // height: Platform.isIOS ? 62 : 56,
                        // height: 56.0,
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0,right: 0),
                          child: AMTextField(text: "Confirm Password",
                            controller: confirmPass,
                            validator: (val)
                            {
                              if(val != pass.text)
                                return 'Not Match';
                              else
                                return null;
                            },
                            //   val.length < 4 ? 'Password too short' : null,

                            obscureText: true,
                          ),

                        ),
                      ),

                      SizedBox(height: 40.0,),
                      Padding(
                        padding: const EdgeInsets.only(left: 80,right: 80),
                        child: CustomButton(
                            onPresed: () {
                              FocusScope.of(context).unfocus();
                              submit(context);
                            },
                            text: "Change Password"),
                      ),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }

  changePassword(User user, BuildContext context) async{
    String password = pass.text;
    user.currentPassword = password;
    User userResp = User(id: user.id, newPassword: password);
    var body = json.encode(userResp);
    String call;
    try{
      var response = await http.post(Uri.parse(AppConstants.ForgetPassword_URL),
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        call = response.body;
        if (call.trim() == "SUCCESS") {
          ShowToast().showToast(context,"Password changed Succesfully");
          MyNavigator.goToLogin(context);
        }
        else if (call.trim() == "NO-USER") {
          ShowToast().showToast(context,"User not found");
        }
        else if (call.trim() == "ERROR") {
          ShowToast().showToast(context,"Unsucessfull");
        }
      }
      else
      {
        ShowToast().showToast(context,"Server Error");
      }
    } on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }
}
