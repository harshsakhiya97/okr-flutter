

import 'dart:convert';
import 'dart:io';
// import 'package:get/get.dart';
import 'package:flutter/services.dart';
// import 'package:http/http.dart' as http;
// import 'package:splash/home/bottom_home.dart';
// import 'package:splash/services/appconstant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
// import 'package:splash/services/models/User.dart';
// import 'package:splash/services/models/UserDto.dart';
// import 'package:splash/services/models/UserLog.dart';
// import 'package:splash/services/show_toast.dart';
// import 'package:splash/utils/my_navigator.dart';
// import 'package:splash/widget/colorCustom.dart';
// import 'package:splash/widget/custom_button.dart';
// import 'package:splash/widget/text_field.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:splash/services/share_pref.dart';

// ignore: must_be_immutable
class VerifyOtp extends StatelessWidget {
  // final User user;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  bool autoValidate = false;
  String otp;
  final TextEditingController txtotp = new TextEditingController();
  final barColor = const Color(0xFF815F8F);

  //VerifyOtp({Key key, this.user}) : super(key: key);



  void submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
    //  verifyotp();
    //  MyNavigator.goToLogin(context);
    }
    else {
//    If all data are not valid then start auto validation.
//     // setState(() {
//        autoValidate = true;
//      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  SingleChildScrollView(
        child: Column(

        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(

                  padding: EdgeInsets.fromLTRB(10.0, 120.0, 10.0, 0.0),
                  child: Center(

                    child: Text(
                      'VERIFY OTP',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 35.0,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          Form(
            key: formKey,
            autovalidate: autoValidate,
            child: Container(
              padding: EdgeInsets.only(top: 80,left: 25,right: 25,bottom: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(

                    color: Colors.transparent,
                    child: Column(

                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 0.0,),
                          child: AMTextField(text: "Enter Otp",
                            controller: txtotp,validator: (val) {
                            if (val.length < 3) {
                              return 'Otp is incorrect';
                            }
                       else
                        return null;
                            },

                            obscureText: false,
                          ),
//
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 5.0),
          Container(
            alignment: Alignment(1.0, 0.0),
            padding: EdgeInsets.only(top:5.0, right: 40.0),
            child: InkWell(
              onTap: (){
              //  sendingOtp(user);
              },
              child: Text(
                'Resend OTP',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Montserrat',
                    decoration: TextDecoration.none),
              ),
            ),
          ),
                  SizedBox(height: 30.0),
                  CustomButton(onPresed: (){
                    submit();
                  }, text: 'Verify OTP',),

                ],
              ),
            ),
          )
        ],
    ),
      ),
    );
  }

  // void verifyotp() {
  //  String actualOtp = user.getOtp();
  //  if(actualOtp == txtotp.text)
  //    {
  //      userRegister();
  //      // registeration
  //
  //    }
  //  else
  //    {
  //      ShowToast().showShortToast("Invalid Otp");
  //    }
  // }

//   sendingOtp(User user) async {
//     var body = json.encode(user);
//     String call;
//     var response = await http.post(AppConstants.Verify_URL,
//         headers: {"Content-Type": "application/json"},
//         body: body);
//     if (response.statusCode == 200) {
//       call = response.body;
//       otp = response.body;
//       user.setOtp(otp);
//     }
//   }
//
//   void userRegister() async {
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     var body = json.encode(user);
//     var jsonResponse;
//     var response = await http.post(AppConstants.Register_URL,
//         headers: {"Content-Type": "application/json"},
//         body: body);
//     if (response.statusCode == 200) {
//       jsonResponse = json.decode(response.body);
//       if(jsonResponse != null) {
//         int userId = (jsonResponse['id']);
//         String userName = (jsonResponse['username']);
//         String userFirstName = (jsonResponse['fullName']);
//         String phone = (jsonResponse['phone']);
//         String email = (jsonResponse['email']);
//         user.setId(userId);
//         user.setUsername(userName);
//         user.setEmail(email);
//         user.setFirstName(userFirstName);
//         user.setPhone(phone);
//         sharedPreferences.setInt("id", user.getId());
//         sharedPreferences.setInt("sync", 0);
//         sharedPreferences.setString("userEmail", user.getEmail());
//         sharedPreferences.setString("firstName", user.getFirstName());
//         sharedPreferences.setString("phone", user.getPhone());
//         sharedPreferences.setString("username", user.getUsername());
//         // sharedPref.save("user",jsonResponse);
//         getAuth(user);
//         print(jsonResponse);
//       }
//       if(jsonResponse == null) {
//         ShowToast().showShortToast("Server Error");
//       }
//     }
//   }
//
//   void getAuth(User user) async{
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     UserDto userDto = new UserDto();
//     userDto.setUsername(user.getEmail());
//     userDto.setPassword(user.getCurrentPassword());
//     var body = json.encode(userDto);
//
//     var jsonResponse;
//     var response = await http.post(AppConstants.LOGIN_URL,
//         headers: {"Content-Type": "application/json"},
//         body:body );
//     if(response.statusCode == 200) {
//       jsonResponse = json.decode(response.body);
//       if(jsonResponse != null) {
//
//         sharedPreferences.setString("token", jsonResponse['token']);
//         //   sharedPreferences.setString("username", userDto.getUsername());
//         // sharedPreferences.setInt("id", userDto.getId());
// //        getuserDetail(user);
//       getUserLog(user.id);
// //         Get.to(BottomHome());
//         // MyNavigator.goToBottom(context);
//         //  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomeScreen()), (Route<dynamic> route) => false);
//       }
//       else if(jsonResponse = null){
//         ShowToast().showShortToast("Server Error");
//       }
//
//     }
//     else {
// //      showShortToast();
//      print("Hello Atul");
//     }
//   }
//
//   void getUserLog(int id) async{
//     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//     String token = sharedPreferences.getString("token");
//     User userResp = new User();
//     userResp.setId(id);
//     UserLogs userLogs = new UserLogs(user: userResp, predefinedName: "login");
//     var body = json.encode(userLogs);
//     var jsonResponse;
//
//     var response = await http.post(AppConstants.log_URL,
//         headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
//         body: body);
//     if (response.statusCode == 200) {
//       jsonResponse = json.decode(response.body);
//       if(jsonResponse != null) {
//         Get.to(BottomHome());
//       }
//       else{
//         ShowToast().showShortToast("Server Problem");
//
//       }
//     }
//     else{
//       ShowToast().showShortToast("Server Problem");
//     }
//   }
}

