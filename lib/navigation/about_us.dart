import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/companyDetail.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  int id;
  String name = "", email= "", phone= "", address = "", filepath = "", description = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      aboutUs();
      getInfo();
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "About Us",)),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 50.0),
          child: Column(

                children: <Widget>[
                  Image(
                    image: AssetImage("images/logo.png"),

                    height: 175.0,
                    width: 250.0,
                  ),

                  // Text(
                  //   'OncoTube',
                  //   style: TextStyle(
                  //     fontFamily: 'Montserrat',
                  //     fontWeight: FontWeight.bold,
                  //     color: customColor().barColor,
                  //     fontSize: 25,
                  //   ),
                  // ),
                  Text(
                    'Welcome',
                    style: TextStyle(
                      fontSize: 20,
                      fontFamily: 'Montserrat',
                      color: customColor().bkColor,
                      letterSpacing: 2.5,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                    width: 200,
                    child: Divider(
                      color: Colors.teal[100],
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  //   child: Center(
                  //     child: Text("Write to us at " + email + " for more information /queries /feedback",textAlign: TextAlign.center,style: TextStyle(
                  //
                  //       fontFamily: 'Montserrat',
                  //       fontWeight: FontWeight.bold,
                  //       fontSize: 18,
                  //       color: Colors.black,
                  //     ),),
                  //   ),
                  // ),
                  Card(
                      color: customColor().greyColor,
                      margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 25.0),
                      child: ListTile(

                        title: Padding(
                          padding: const EdgeInsets.only(top: 20,bottom: 20),
                          child: Html(
                            data: description != null ? description: "OKR"
                        ),
                        ),
                      )),

                ],
              ),
        ),
      ),



    );

  }
  void getInfo() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      email= sharedPreferences.getString("CompEmail").toString();
      description= sharedPreferences.getString("CompDesc").toString();
    });
  }
  void aboutUs() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    CompanyDetail companyDetail = new CompanyDetail();
    var body = json.encode(companyDetail);
    var jsonResponse;
    var url = Uri.parse(AppConstants.AboutUs_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
          id = (jsonResponse["companyDetailId"]);
          name = (jsonResponse["name"]);
         phone = (jsonResponse["phone"]);
          email = (jsonResponse["email"]);
          address = (jsonResponse["address"]);
          description = (jsonResponse["description"]);
          filepath = (jsonResponse["filePath"]);

          setState(() {

          });
      }
    }
  }
}
