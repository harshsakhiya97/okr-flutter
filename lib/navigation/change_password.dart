import 'dart:convert';

import 'dart:io';

import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/models/User.dart';
import '../services/share_pref.dart';


class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  // UserDto userDto = UserDto();
  bool autoValidate = false;
  final TextEditingController oldpass = TextEditingController();
  final TextEditingController pass = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();
  final barColor = const Color(0xFF815F8F);
  User user;
  void submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
     changePassword(oldpass.text, pass.text);
    }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        autoValidate = true;
      });
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
  }
  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {

        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Change Password",)),
    body: SingleChildScrollView(
    child: Column(
    children: <Widget>[
    Container(
    child: Stack(
    children: <Widget>[
    Container(

    padding: EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
    child: Center(
    child: Text(
    'Change Password',
    textAlign: TextAlign.center,
    style: TextStyle(
    fontFamily: 'Montserrat',
    fontSize: 35.0,
    fontWeight: FontWeight.bold,
    ),
    ),
    ),
    ),
    ],
    ),
    ),
    Form(
      key: formKey,
      autovalidate: autoValidate,
      child: Container(

      padding: EdgeInsets.only(top: 60,left: 25,right: 25),
      child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
      Container(
      // height: Platform.isIOS ? 62 : 56,
      // height: 56.0,
      color: Colors.transparent,
      child: Padding(
      padding: const EdgeInsets.only(left: 0,right: 0),
                  child: AMTextField(text: "Old Password",readOnly: false, obscureText: true,
                    controller: oldpass,
      validator: (val) {
      if( val.length < 5 ) {
      return 'Password too short';
      }
      if( val.length >= 15 ) {
      return 'Password too big';
      }
      else
      return null;
      },

                  ),

      ),
      ),
      SizedBox(height: 15.0,),
      Container(
      // height: Platform.isIOS ? 62 : 56,
      // height: 56.0,
      color: Colors.transparent,
      child: Padding(
      padding: const EdgeInsets.only(left: 0,right: 0),
            child: AMTextField(text: "New Password",readOnly: false,
              controller: pass,
      validator: (val)
      {
        if( val.length < 5 ) {
          return 'Password too short';
        }
        if( val.length >= 15 ) {
          return 'Password too big';
        }
        else
          return null;
      },
      //   val.length < 4 ? 'Password too short' : null,

      obscureText: true,
            ),

      ),
      ),
        SizedBox(height: 15.0,),
        Container(
          // height: Platform.isIOS ? 62 : 56,
          // height: 56.0,
          color: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.only(left: 0,right: 0),
            child: AMTextField(text: "Confirm Password",readOnly: false,
              controller: confirmPass,
              validator: (val)
              {
                if(val != pass.text)
                  return 'Not Match';
                else
                  return null;
              },
              //   val.length < 4 ? 'Password too short' : null,

              obscureText: true,
            ),

          ),
        ),


        SizedBox(height: 40.0,),
      Padding(
        padding: const EdgeInsets.only(left: 0,right: 0),
        child: CustomButton(
        onPresed: () {
         submit();
        },
        text: "Change Password"),
      ),

      ],
      ),
      ),
    ),
    ],
    ),
    ),
    );


  }

  void changePassword(String oldpassword, String newpassword) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    User userTemp = new User(username: user.username, currentPassword:  oldpassword, newPassword: newpassword);

    String token = sharedPreferences.getString("token");
    var body = json.encode(userTemp);
    String call;
    try {
      var url = Uri.parse(AppConstants.ChangePassword_URL);
      var response = await http.put(url,
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          },
          body: body);
      if (response != null && response.statusCode == 200) {
        call = response.body;
        if (call.trim() == "SUCCESS") {
          oldpass.clear();
          confirmPass.clear();
          pass.clear();
          ShowToast().showToast(context,"Password changed Succesfully");
          // userDto.setPassword(user.getNewPassword());
          // user.setCurrentPassword(user.getNewPassword());
        }
        else if (call.trim() == "INCORRECT") {
          ShowToast().showToast(context,"Old Password is incorrect");
        }
        else if (call.trim() == "ERROR") {
          ShowToast().showToast(context,"Unsucessfull");
        }
      }
      else
      {
        ShowToast().showToast(context,"Server Error");
      }
    }on Exception catch (_){
      ShowToast().showToast(context,"Check Your Internet Connection");
    }

  }
}
