
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:okr/home/edit_profile.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:shared_preferences/shared_preferences.dart';


class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  String title = "", email = "", phone = "", department = "", industry = "", designation = "", designationLevel = "", region = "";
  User user;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
   // loadname();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "My Profile",)),

      body: SingleChildScrollView(
        child: Column(
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(

                      padding: EdgeInsets.fromLTRB(0.0, 80.0, 0.0, 0.0),
                      child: Center(
                        child: Text(
                          'My Profile',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 35.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
              SizedBox(height: 40,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      Icons.account_circle,
                      color: customColor().bkColor,
                    ),
                    title: Text(
                      title,
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      Icons.phone,
                      color: customColor().bkColor,
                    ),
                    title: Text(
                      phone,
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      Icons.email,
                      color: customColor().bkColor,
                    ),
                    title: Text(
                      email,
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      Icons.business,
                      color: customColor().bkColor,
                    ),
                    title: Text( department.isNotEmpty ? department :
                      "Department",
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      MdiIcons.officeBuilding,
                      color: customColor().bkColor,
                    ),
                    title: Text( industry.isNotEmpty ? industry :
                      "Industry",
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      MdiIcons.bank,
                      color: customColor().bkColor,
                    ),
                    title: Text(designation.isNotEmpty ? designation :
                      "Designation",
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Card(
                  color: Colors.white,
                  margin:
                  EdgeInsets.only(left: 20,right: 20),
                  child: ListTile(
                    leading: Icon(
                      MdiIcons.bankPlus,
                      color: customColor().bkColor,
                    ),
                    title: Text(designationLevel.isNotEmpty ? designationLevel :
                      "Designation Level",
                      style:
                      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0),
                    ),
                  )),
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: CustomButton(onPresed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => EditProfile(
                        user: user,
                      ),),);
                }, text: "Edit"),
              )
//            Text("Name",
//            style: TextStyle(
//                fontSize: 18,
//                color: Colors.black,
//                fontWeight: FontWeight.bold,
//                fontFamily: 'Montserrat',
//            ),),
//            SizedBox(height: 10,)
            ],
          ),
      ),

    );
  }
  // void loadname() async{
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     title = sharedPreferences.getString("firstName").toString();
  //     email = sharedPreferences.getString("userEmail").toString();
  //     phone = sharedPreferences.getString("phone").toString();
  //   });
  // }

  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        if(user != null) {
          if (user.firstName.isNotEmpty) {
            title = user.firstName.toString();
          }
          if (user.phone.isNotEmpty) {
            phone = user.phone.toString();
          }
          if (user.email.isNotEmpty) {
            email = user.email.toString();
          }
          if (user.designationName.isNotEmpty) {
            designation = user.designationName.toString();
          }
          if (user.designationLevelName.isNotEmpty) {
            designationLevel = user.designationLevelName.toString();
          }
          if (user.departmentName.isNotEmpty) {
            department = user.departmentName.toString();
          }
          if (user.industryName.isNotEmpty) {
            industry = user.industryName.toString();
          }
          if (user.regionName.isNotEmpty) {
            region = user.regionName.toString();
          }
          // if (user.email.isNotEmpty) {
          //   email = user.email.toString();
          // }
        }
        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }
}
