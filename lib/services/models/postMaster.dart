import 'dart:convert';


import '../ContentMaster.dart';
import 'User.dart';

class PostMaster{
int postId, minDays, maxDays, lastPostId, recordSize, bookmarkId, currentUserBookmarkId, is_bookmared;
String lilacCode, panelName, methodology, specimenRequirements, testMarkers, commonFilter, filePathThumbnail, filePathnamePdf, filePath, categoryName, subCategoryName, diseases;
double price;
bool currentUserBookmark;

User currentUser;
// List<BookmarkMaster> bookmarkMaster = new List<BookmarkMaster>();
// List<ContentMaster> contentMasterList = new List<ContentMaster>();

PostMaster({this.postId, this.lilacCode, this.panelName, this.methodology, this.specimenRequirements, this.testMarkers, this.maxDays,this.minDays, this.price, this.commonFilter, this.filePathThumbnail, this.lastPostId, this.recordSize, this.bookmarkId, this.filePathnamePdf, this.currentUser, this.currentUserBookmark, this.currentUserBookmarkId, this.filePath, this.categoryName, this.subCategoryName, this.is_bookmared, this.diseases});
        PostMaster.fromJson(Map<String, dynamic> json)
            : postId = json['postId'],
              bookmarkId = json['bookmarkId'],
              minDays = json['minDays'],
              maxDays = json['maxDays'],
              lilacCode = json['lilacCode'],
              panelName = json['panelName'],
              methodology = json['methodology'],
              specimenRequirements = json['specimenRequirements'],
              testMarkers = json['testMarkers'],
              commonFilter = json['commonFilter'],
             // contentMasterList = json['contentMasterList'],
              filePathThumbnail = json['filePathThumbnail'],
              filePath = json['filePath'],
              diseases = json['diseases'],
              categoryName = json['categoryName'],
              subCategoryName = json['subCategoryName'],
            lastPostId = json['lastPostId'],
              recordSize = json['recordSize'],
              filePathnamePdf = json['filePathnamePdf'],
              price = json['price'],
              is_bookmared = json['is_bookmared'],
              currentUserBookmarkId = json['currentUserBookmarkId'],
              currentUserBookmark = json['currentUserBookmark'];
       Map<String, dynamic> toJson() =>
        {
          'postId': postId,
          'bookmarkId': bookmarkId,
           'minDays': minDays,
          'maxDays': maxDays,
          'lilacCode': lilacCode,
          'panelName': panelName,
          'methodology': methodology,
          'specimenRequirements': specimenRequirements,
           'testMarkers': testMarkers,
          'commonFilter':commonFilter,
          'price': price,
          'is_bookmared': is_bookmared,
          'currentUser': currentUser,
          'filePathThumbnail' : filePathThumbnail,
          'diseases' : diseases,
          'lastPostId' : lastPostId,
          'recordSize'  : recordSize,
          'filePathnamePdf' : filePathnamePdf,
          'currentUserBookmarkId' : currentUserBookmarkId,
          'currentUserBookmark' : currentUserBookmark,
          'filePath' : filePath,
          'categoryName' : categoryName,
          'subCategoryName' : subCategoryName,
    };

void setPostId(int getPostId)
{
  postId = getPostId;
}

//is_bookmared
  void setIs_bookmared(int getIs_bookmared)
  {
    is_bookmared = getIs_bookmared;
  }
void setMinDays(int getMinDays)
{
  minDays = getMinDays;
}

void setBookmarkId(int getBookmarkId)
{
  bookmarkId = getBookmarkId;
}

void setMaxDays(int getMaxDays)
{
  maxDays = getMaxDays;
}

void setLastPostId(int getLastPostId)
{
  lastPostId = getLastPostId;
}

void setRecordSize(int getRecordSize)
{
  recordSize = getRecordSize;
}

void setPrice(double getPrice)
{
  price = getPrice;
}
void setCurrentUser(User getCurrentUser)
{
  currentUser= getCurrentUser;
}

void setCategoryName(String getCategoryName)
{
  categoryName = getCategoryName;
}

  void setDiseases(String getDiseases)
  {
    diseases = getDiseases;
  }

void setSubCategoryName(String getSubCategoryName)
{
  subCategoryName = getSubCategoryName;
}

void setLilacCode(String getLilacCode)
{
  lilacCode = getLilacCode;
}

void setCurrentUserBookmarkId(int getCurrentUserBookmarkId)
{
  currentUserBookmarkId = getCurrentUserBookmarkId;
}

void setCurrentUserBookmark(bool getCurrentUserBookmark)
{
  currentUserBookmark = getCurrentUserBookmark;
}

void setPanelName(String getPanelName)
{
  panelName = getPanelName;
}

void setMethodology(String getMethodology)
{
  methodology = getMethodology;
}

void setSpecimenRequirements(String getSpecimenRequirements)
{
  specimenRequirements = getSpecimenRequirements;
}

void setTestMarkers(String getTestMarkers)
{
  testMarkers = getTestMarkers;
}

void setCommonFilter(String getCommonFilter)
{
  commonFilter = getCommonFilter;
}

void setFilePathThumbnail(String getFilePathThumbnail)
{
  filePathThumbnail = getFilePathThumbnail;
}

// void setContentMasterList(List getContentMasterList)
// {
//   contentMasterList = getContentMasterList;
// }
//
// void setBookmarkMaster(List getBookmarkMaster) {
//   bookmarkMaster = getBookmarkMaster;
// }

  void setFilePathnamePdf(String getFilePathnamePdf)
  {
    filePathnamePdf = getFilePathnamePdf;
  }

void setFilePath(String getFilePath)
{
  filePath = getFilePath;
}

//filePath
//filePathnamePdf
//bookmarkMaster
//contentMasterList
int getPostId()
{
  return postId;
}
//is_bookmared
  int getIs_bookmared()
  {
    return is_bookmared;
  }
  int getBookmarkId()
  {
    return bookmarkId;
  }

int getMinDays()
{
  return minDays;
}

int getMaxDays()
{
  return maxDays;
}

int getLastPostId()
{
  return lastPostId;
}

 bool getCurrentUserBookmark()
  {
    return currentUserBookmark;
  }

  int getCurrentUserBookmarkId()
  {
    return currentUserBookmarkId;
  }

int getRecordSize()
{
  return recordSize;
}

double getPrice()
{
  return price;
}

String getLilacCode()
{
  return lilacCode;
}

String getPanelName()
{
  return panelName;
}

  String getDiseases()
  {
    return diseases;
  }

String getMethodology()
{
  return methodology;
}

  String getCategoryName()
  {
    return categoryName;
  }

  String getSubCategoryName()
  {
    return subCategoryName;
  }

String getFilePathnamePdf()
{
  return filePathnamePdf;
}

  String getFilePath()
  {
    return filePath;
  }


String getSpecimenRequirements()
{
  return specimenRequirements;
}

String getTestMarkers()
{
  return testMarkers;
}

String getCommonFilter()
{
  return commonFilter;
}

  String getFilePathThumbnail()
  {
    return filePathThumbnail;
  }

  // List getContentMasterList()
  // {
  //   return contentMasterList;
  // }
  //
  // List getBookmarkMaster()
  // {
  //   return bookmarkMaster;
  // }

  User getCurrentUser()
  {
    return currentUser;
  }


}


