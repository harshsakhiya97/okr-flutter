
import 'package:json_annotation/json_annotation.dart';


part 'UserDto.g.dart';

@JsonSerializable()
class UserDto {

  String username;
  String password;

  UserDto(this.username, this.password);

  factory UserDto.fromJson(Map<String, dynamic> json) => _$UserDtoFromJson(json);
  Map<String, dynamic> toJson() => _$UserDtoToJson(this);
}