class SubCategory{
  int predefinedId, parentPredefinedId;
  String entityType, name, sortOrder, parentName;
  SubCategory({this.name, this.entityType, this.predefinedId, this.sortOrder, this.parentName, this.parentPredefinedId});

  SubCategory.fromJson(Map<String, dynamic> json)
      : predefinedId = json['predefinedId'],
        entityType = json['entityType'],
        name = json['name'],
        sortOrder = json['sortOrder'],
        parentName = json['parentName'],
        parentPredefinedId = json['parentPredefinedId'];
  Map<String, dynamic> toJson() =>
      {
        'predefinedId': predefinedId,
        'entityType': entityType,
        'name': name,
        'sortOrder': sortOrder,
        'parentName' : parentName,
        'parentPredefinedId' : parentPredefinedId,
      };

  void setPredefinedId(int getPredefinedId)
  {
    predefinedId = getPredefinedId;
  }

  void setParentPredefinedId(int getParentPredefinedId)
  {
    parentPredefinedId = getParentPredefinedId;
  }

  void setEntityType(String getEntityType)
  {
    entityType = getEntityType;
  }

  void setName(String getName)
  {
    name = getName;
  }

  void setParentName(String getParentName)
  {
    parentName = getParentName;
  }

  void setSortOrder(String getSortOrder)
  {
    sortOrder = getSortOrder;
  }

  int getPredefinedId()
  {
    return predefinedId;
  }

  int getParentPredefinedId()
  {
    return parentPredefinedId;
  }

  String getEntityType()
  {
    return entityType;
  }

  String getName()
  {
    return name;
  }

  String getParentName()
  {
    return parentName;
  }

  String getSortOrder()
  {
    return sortOrder;
  }


}