import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:okr/services/models/category.dart';

import 'User.dart';

class SaveMaster{
  int savedSearchMasterId, okrCounts, departmentId, subDepartmentId;
  String searchName, departmentName, subDepartmentName, isDelete, isDeactive;
  User createdBy;
  Category department, subDepartment;

  SaveMaster({
      this.savedSearchMasterId,
      this.okrCounts,
      this.departmentId,
      this.subDepartmentId,
      this.searchName,
      this.departmentName,
      this.subDepartmentName,
      this.isDelete,
      this.isDeactive,
      this.createdBy,
      this.department,
      this.subDepartment});

  SaveMaster.fromJson(Map<String, dynamic> json)
      : savedSearchMasterId = json['savedSearchMasterId'],
        okrCounts = json['okrCounts'],
        searchName = json['searchName'],
        subDepartmentName = json['subDepartmentName'],
        isDelete = json['isDelete'],
        departmentName = json['departmentName'],
        departmentId = json['departmentId'],
        subDepartmentId = json['subDepartmentId'],
        createdBy = User.fromJson(json['createdBy']),
        isDeactive = json['isDeactive'];


  Map<String, dynamic> toJson() =>
      {
        'savedSearchMasterId': savedSearchMasterId,
        'okrCounts': okrCounts,
        'departmentId': departmentId,
        'subDepartmentId': subDepartmentId,
        'searchName': searchName,
        'departmentName': departmentName,
        'subDepartmentName': subDepartmentName,
        'createdBy': createdBy,
        'isDeactive'  : isDeactive,
        'isDelete'  : isDelete,
        'department' : department,
        'subDepartment' : subDepartment,
      };

}