import 'dart:core';

class CompanyDetail
{
  int companyDetailId;
  String name, description, email, phone, address, facebookPage, instagramPage, twitterPage, filePath;
  CompanyDetail({this.email, this.name, this.phone, this.address, this.companyDetailId, this.description, this.facebookPage, this.filePath, this.instagramPage,this.twitterPage});
      CompanyDetail.fromJson(Map<String, dynamic> json)
          : companyDetailId = json['companyDetailId'],
            name = json['name'],
            description = json['description'],
            email = json['email'],
            phone = json['phone'],
            address = json['address'],
            facebookPage = json['facebookPage'],
            instagramPage = json['instagramPage'],
            twitterPage = json['twitterPage'],
            filePath = json['filePath'];

  Map<String, dynamic> toJson() =>
      {
        'companyDetailId': companyDetailId,
        'name': name,
        'description': description,
        'email': email,
        'phone': phone,
        'address': address,
        'facebookPage': facebookPage,
        'instagramPage': instagramPage,
        'twitterPage': twitterPage,
        'filePath': filePath,
      };

  void setCompanyDetailId(int getCompanyDetailId)
  {
    companyDetailId = getCompanyDetailId;
  }

  void setName(String getName)
  {
    name = getName;
  }

  void setDescription(String getDescription)
  {
    description = getDescription;
  }

  void setEmail(String getEmail)
  {
    email = getEmail;
  }

  void setPhone(String getPhone)
  {
    phone = getPhone;
  }

  void setAddress(String getAddress)
  {
    address = getAddress;
  }

  void setFilePath(String getFilePath)
  {
    filePath = getFilePath;
  }
  //filePath

  int getCompanyDetailId()
  {
    return companyDetailId;
  }

  String getName()
  {
    return name;
  }
  String getDescription()
  {
    return description;
  }

  String getEmail()
  {
    return email;
  }

  String getPhone()
  {
    return phone;
  }

  String getAddress()
  {
    return address;
  }

  String getFilePath()
  {
    return filePath;
  }




}