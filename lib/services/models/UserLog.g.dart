// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserLog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserLogs _$UserLogsFromJson(Map<String, dynamic> json) {
  return UserLogs(
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    eventId: json['eventId'] as int,
    testId: json['testId'] as int,
    pageSize: json['pageSize'] as int,
    pageNumber: json['pageNumber'] as int,
    logId: json['logId'] as int,
    userId: json['userId'] as int,
    predefinedName: json['predefinedName'] as String,
    eventName: json['eventName'] as String,
    userName: json['userName'] as String,
    testName: json['testName'] as String,
    isDeactive: json['isDeactive'] as String,
    isDelete: json['isDelete'] as String,
    test: json['test'] == null
        ? null
        : PostMaster.fromJson(json['test'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserLogsToJson(UserLogs instance) => <String, dynamic>{
      'eventId': instance.eventId,
      'testId': instance.testId,
      'pageSize': instance.pageSize,
      'pageNumber': instance.pageNumber,
      'logId': instance.logId,
      'userId': instance.userId,
      'predefinedName': instance.predefinedName,
      'eventName': instance.eventName,
      'userName': instance.userName,
      'testName': instance.testName,
      'isDeactive': instance.isDeactive,
      'isDelete': instance.isDelete,
      'test': instance.test,
      'user': instance.user,
    };
