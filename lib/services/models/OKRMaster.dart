
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/category.dart';

class OKRMaster{
  int okrMasterId, totalDownloads, totalAccessed, pageSize, pageNumber, okrCounts, bookmarkId;
  String objective, keyResult, searchValue, designationLevelName, departmentName, subDepartmentName, designationName, industryName, header;
  User currentUser;
  bool isActive;
  Category department, subDepartment;
  OKRMaster({
      this.okrMasterId,
      this.totalDownloads,
      this.totalAccessed,
      this.pageSize,
      this.pageNumber,
      this.objective,
      this.keyResult,
      this.searchValue,
      this.designationLevelName,
      this.departmentName,
      this.subDepartmentName,
      this.designationName,
      this.okrCounts,
      this.header,
      this.bookmarkId,
      this.currentUser,
      this.industryName,
      this.department,
      this.subDepartment,
      this.isActive});

  OKRMaster.fromJson(Map<String, dynamic> json)
      : okrMasterId = json['okrMasterId'],
        totalDownloads = json['totalDownloads'],
        totalAccessed = json['totalAccessed'],
        pageSize = json['pageSize'],
        pageNumber = json['pageNumber'],
        objective = json['objective'],
        keyResult = json['keyResult'],
        searchValue = json['searchValue'],
        designationLevelName = json['designationLevelName'],
        departmentName = json['departmentName'],
        subDepartmentName = json['subDepartmentName'],
        designationName = json['designationName'],
        okrCounts = json['okrCounts'],
        industryName = json['industryName'],
        bookmarkId = json['bookmarkId'];

  Map<String, dynamic> toJson() =>
      {
        'okrMasterId': okrMasterId,
        'totalDownloads': totalDownloads,
        'totalAccessed': totalAccessed,
        'pageSize': pageSize,
        'pageNumber': pageNumber,
        'objective': objective,
        'keyResult': keyResult,
        'searchValue': searchValue,
        'designationLevelName': designationLevelName,
        'departmentName':departmentName,
        'subDepartmentName': subDepartmentName,
        'designationName': designationName,
        'industryName': industryName,
        'okrCounts': okrCounts,
        'bookmarkId': bookmarkId,
        'currentUser': currentUser,
        'department' : department,
        'subDepartment' : subDepartment,
      };


}