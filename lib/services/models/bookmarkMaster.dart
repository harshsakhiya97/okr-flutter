import 'dart:convert';

import 'package:okr/services/models/OKRMaster.dart';

import 'User.dart';



class BookmarkMaster{
  int bookmarkId, okrMasterId, createdById, lastBookmarkId, pageSize, pageNumber, recordSize;
  User currentUser, createdBy;
  String isDelete;
  OKRMaster okrMaster;
  BookmarkMaster({this.recordSize, this.okrMasterId, this.bookmarkId, this.createdBy, this.createdById, this.currentUser,this.lastBookmarkId, this.okrMaster, this.isDelete, this.pageSize, this.pageNumber});
          BookmarkMaster.fromJson(Map<String, dynamic> json)
              : okrMasterId = json['okrMasterId'],
                bookmarkId = json['bookmarkId'],
                pageSize = json['pageSize'],
                pageNumber = json['pageNumber'],
                createdById = json['createdById'],
                lastBookmarkId = json['lastBookmarkId'],
                isDelete = json['isDelete'],
                okrMaster = OKRMaster.fromJson(json['okrMaster']),
                createdBy = User.fromJson(json['createdBy']),
                recordSize = json['recordSize'];


  Map<String, dynamic> toJson() =>
      {
        'okrMasterId': okrMasterId,
        'bookmarkId': bookmarkId,
        'pageSize': pageSize,
        'pageNumber': pageNumber,
        'createdById': createdById,
        'lastBookmarkId': lastBookmarkId,
        'currentUser': currentUser,
        'createdBy': createdBy,
        'recordSize'  : recordSize,
          'isDelete'  : isDelete,
        'okrMaster' : okrMaster,
      };


}