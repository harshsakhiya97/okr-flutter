class Category{
  int predefinedId, parentPredefinedId;
  String entityType, name, sortOrder;
  Category({this.name, this.entityType, this.predefinedId, this.sortOrder, this.parentPredefinedId});

        Category.fromJson(Map<String, dynamic> json)
            : predefinedId = json['predefinedId'],
              entityType = json['entityType'],
              name = json['name'],
              parentPredefinedId = json['parentPredefinedId'],
              sortOrder = json['sortOrder'];

    Map<String, dynamic> toJson() =>
      {
        'predefinedId': predefinedId,
        'entityType': entityType,
        'name': name,
        'sortOrder': sortOrder,
        'parentPredefinedId': parentPredefinedId,
      };

  void setPredefinedId(int getPredefinedId)
  {
    predefinedId = getPredefinedId;
  }

  void setEntityType(String getEntityType)
  {
    entityType = getEntityType;
  }

  void setName(String getName)
  {
    name = getName;
  }

  void setSortOrder(String getSortOrder)
  {
    sortOrder = getSortOrder;
  }

  int getPredefinedId()
  {
    return predefinedId;
  }

  String getEntityType()
  {
    return entityType;
  }

  String getName()
  {
    return name;
  }

  String getSortOrder()
  {
    return sortOrder;
  }

}