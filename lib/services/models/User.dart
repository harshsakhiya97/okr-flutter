import 'package:json_annotation/json_annotation.dart';
import 'package:okr/services/models/category.dart';

@JsonSerializable()
class User {

  int id, departmentId;
  Category industry, designation, designationLevel, department, region;
   String username, email, firstName, currentPassword, phone, otp, newPassword, fireBaseId, companyName,lastName, regionName, departmentName, designationLevelName, designationName, industryName;
  User({this.id, this.username, this.email, this.firstName, this.currentPassword,
      this.phone, this.otp, this.newPassword, this.fireBaseId,this.companyName, this.lastName, this.departmentName, this.designationLevelName, this.designationName, this.industryName, this.regionName,
      this.designationLevel, this.region, this.department, this.designation, this.industry, this.departmentId});

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        username = json['username'],
        email = json['email'],
        phone = json['phone'],
        firstName = json['firstName'],
        companyName = json['companyName'],
        lastName = json['lastName'],
        currentPassword = json['currentPassword'],
        newPassword = json['newPassword'],
        fireBaseId = json['fireBaseId'],
        departmentName = json['departmentName'],
        designationLevelName = json['designationLevelName'],
        designationName = json['designationName'],
        industryName = json['industryName'],
        regionName = json['regionName'],
        departmentId = json['departmentId'],
        otp = json['otp'];




  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'username': username,
        'email': email,
        'phone': phone,
        'firstName': firstName,
        'companyName' : companyName,
        'lastName' : lastName,
        'currentPassword': currentPassword,
        'otp': otp,
        'newPassword': newPassword,
        'fireBaseId' : fireBaseId,
        'departmentName' : departmentName,
        'designationLevelName' : designationLevelName,
        'designationName' : designationName,
        'industryName' : industryName,
        'regionName' : regionName,
        'designationLevel' : designationLevel,
        'region' : region,
        'department' : department,
        'designation' : designation,
        'industry' : industry,
        'departmentId' : departmentId,
      };





}