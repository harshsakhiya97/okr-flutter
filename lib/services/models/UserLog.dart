import 'package:json_annotation/json_annotation.dart';
import 'package:okr/services/models/postMaster.dart';

import 'User.dart';

part 'UserLog.g.dart';

@JsonSerializable()
class UserLogs{
  int eventId, testId, pageSize, pageNumber, logId, userId;
  String predefinedName, eventName, userName, testName, isDeactive, isDelete;
  PostMaster test;
  User user;
  UserLogs({this.user, this.eventId, this.testId, this.pageSize, this.pageNumber, this.logId, this.userId, this.predefinedName,this.eventName, this.userName, this.testName, this.isDeactive, this.isDelete, this.test});


  factory UserLogs.fromJson(Map<String, dynamic> json) =>
      _$UserLogsFromJson(json);
  Map<String, dynamic> toJson() => _$UserLogsToJson(this);

}