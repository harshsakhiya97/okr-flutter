import 'package:okr/services/models/OKRMaster.dart';
import 'package:okr/services/models/User.dart';

class DownloadItem{
  int downloadItemId;
  String errorMessage;
  User user;
  List<OKRMaster> okrMasterListTemp;

  DownloadItem({this.downloadItemId, this.errorMessage, this.user,
      this.okrMasterListTemp});
  DownloadItem.fromJson(Map<String, dynamic> json)
      : errorMessage = json['errorMessage'];

  Map<String, dynamic> toJson() =>
      {
        'user': user,
        'okrMasterListTemp': okrMasterListTemp,
      };
}