import 'User.dart';

class NotificationMaster{
  String title, message,createdDate;
  int lastNotificationId, notificationId;
  User  createdFor;
  NotificationMaster({this.title,this.message,this.createdFor, this.createdDate, this.lastNotificationId, this.notificationId});
  NotificationMaster.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        message = json['message'],
        createdDate = json['createdDate'],
        notificationId = json['notificationId'],
        lastNotificationId = json['lastNotificationId'];
  Map<String, dynamic> toJson() =>
      {
        'title': title,
        'message': message,
        'createdFor' : createdFor,
        'createdDate' : createdDate,
        'lastNotificationId' : lastNotificationId,
        'notificationId' : notificationId,
      };

  void setCreatedFor(User getCreatedFor)
  {
    createdFor = getCreatedFor;
  }

  void setTitle(String getTitle)
  {
    title = getTitle;
  }

  void setMessage(String getMessage)
  {
    message = getMessage;
  }

  void setCreatedDate(String getCreatedDate)
  {
    createdDate = getCreatedDate;
  }

  void setLastNotificationId(int getLastNotificationId)
  {
    lastNotificationId = getLastNotificationId;
  }

  void setNotificationId(int getNotificationId)
  {
    notificationId = getNotificationId;
  }
//notificationId
  int getNotificationId()
  {
    return notificationId;
  }

  int getLastNotificationId()
  {
    return lastNotificationId;
  }
  String getTitle()
  {
    return title;
  }

  String getMessage()
  {
    return message;
  }

  String getCreatedDate()
  {
    return createdDate;
  }

  User getCreatedFor()
  {
    return createdFor;
  }

}