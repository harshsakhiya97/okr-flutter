 class AppConstants {
    static final  BASE_URL   = "http://65.0.103.13:8080/OKRServer/";
    // static final String BASE_URL   = "http://15.207.190.28:8080/LilacServer/";
    static final LOGIN_URL = BASE_URL + "token/generate-token";
    static final Check_URL = BASE_URL + "user/checkDuplicateEmailAndMobileNo";
    static final Verify_URL = BASE_URL + "user/sendOtpToUser";
    static final Register_URL = BASE_URL + "user/userRegistration";
    static final UserDetail_URL = BASE_URL + "user/getUserDetailsByUsername";
    static final UserDetailById_URL = BASE_URL + "user/getUserById";
    static final ForgetPassword_URL = BASE_URL + "user/forgetPassword";
    static final ChangePassword_URL = BASE_URL + "user/changePassword";
    static final OKRMasterList_URL = BASE_URL + "okr/getOkrMasterList";
    static final TestDetail_URL = BASE_URL + "post/getPostById";
    static final BookMarkList_URL = BASE_URL + "bookmark/getBookmarkListByUser";
    static final SaveSearchList_URL = BASE_URL + "savedSearch/getSavedSearchMasterList";
    static final DeleteBookMark_URL = BASE_URL + "bookmark/deleteBookmark";
    static final DeleteSaveSearch_URL = BASE_URL + "savedSearch/deleteSavedSearch";
    static final SaveBookMark_URL = BASE_URL + "bookmark/saveBookmark";
    static final SaveSearch_URL = BASE_URL + "savedSearch/saveSavedSearch";
    static final AboutUs_URL = BASE_URL + "companyDetail/getCompanyDetail";
    static final Category_URL = BASE_URL + "predefined/getPredefinedListByType";
   // static final SaveCategory_URL = BASE_URL + "predefined/savePredefinedWithFile";
    static final GetNotification_URL = BASE_URL + "notification/getNotificationListByUser";
    static final UpdateNotification_URL = BASE_URL + "notification/updateNotification";
    static final TestCount_URL = BASE_URL + "user/totalUserCount";
    static final Firebase_URL = BASE_URL + "user/updateFirebase";
    static final countNotification_URL = BASE_URL + "notification/getUnseenNotificationCountByUser";
    static final AllTest_URL = BASE_URL + "post/totalPostCount";
    static final log_URL = BASE_URL + "userlog/saveUserLogs";
    static final UpdateUser_URL = BASE_URL + "user/updateUserDetail";
    static final Download_URL = BASE_URL + "downloadItem/saveMultipleDownloadItem";





//notification/getUnseenNotificationCountByUser
 //user/updateFirebase

//user/totalUserCount
}