import 'package:flutter/material.dart';
import 'package:okr/widget/fluttertoast.dart';
class ShowToast {

  showToast(BuildContext context, String msg) {
    Widget toast = Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25.0),
            color: Colors.grey.shade500,
          ),
          child: Center(
              child: Text(
                msg,
                maxLines: 3,
                textAlign: TextAlign.center,
              )),
        ));
    FlutterToast(context).showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 1),
    );
  }
}



