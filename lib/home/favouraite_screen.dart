import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/OKRMaster.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/bookmarkMaster.dart';
import 'package:okr/services/models/downloaditem.dart';
import 'package:okr/services/models/postMaster.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_box.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
class Favouraite extends StatefulWidget {
  @override
  _FavouraiteState createState() => _FavouraiteState();
}

class _FavouraiteState extends State<Favouraite> {
  bool valuefirst = true;
  var bookmark = Icons.bookmark_border;
  int defaultRecordSize = 7;
  int latestId =0;
  bool isLoading = false;
  List<OKRMaster> okrMasterList = [];
  List<OKRMaster> okrMasterTempList = [];
  ScrollController _scrollController = ScrollController();
  String prevSearched;
  User user;
  List<BookmarkMaster> bookMasterList = [];
  final TextEditingController txtsearch = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
      bookmarkList();
    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent)
      {
        latestId++;
        bookmarkList();
      }
    });
  }

  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
       // print(user.id);
      });
    } catch (Excepetion) {
      // do something
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Favorite",)),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          border: Border.all(

                              color: Colors.grey,
                              style: BorderStyle.solid,
                              width: 1.0),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40.0)),
                      child: Column(
                        children: <Widget>[

                          GestureDetector(

                            child: Padding(
                              padding: EdgeInsets.only(left: 0.0,),

                              child: TextField(
                                autofocus: false,
                                controller: txtsearch,
                                onChanged: (value) => updateSearchState(value),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Search for OKRs',
                                  hintStyle: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'Montserrat',
                                    color: Colors.grey,

                                  ),
                                  icon:
                                  IconButton(
                                    icon: Icon(Icons.search, size: 15,),
                                    onPressed: (){
                                      updateSearchState(txtsearch.text.toString());
                                      // MyNavigator.goToSearch(context);
                                    },
                                  ),
                                ),
                              ),

                            ),
                            // onTap: (){MyNavigator.goToSearch(context);},
                          ),
                        ],
                      ),

                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, right: 15, bottom: 15),
                  child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),color: customColor().logoColor,), child: Padding(
                    padding: const EdgeInsets.only(left: 2, top: 2, bottom: 3, right: 3),
                    child: IconButton(icon: Icon(Icons.download_rounded,size: 20,color: Colors.white), onPressed: (){
                      //  getDownload();
                      if(okrMasterTempList != null && okrMasterTempList.length > 0)
                      {
                        DownloadItem downloadItem = DownloadItem(user: user, okrMasterListTemp: okrMasterTempList);
                        sendDownload(downloadItem);
                      }else{
                        ShowToast().showToast(context, "Select CheckBox to Download Report");
                      }
                    }),
                  ),),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: bookMasterList.length,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              physics: ScrollPhysics(),
              controller: _scrollController,
              itemBuilder: (context, i) => new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                          ),

                          borderRadius: BorderRadius.circular(10.0)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Checkbox(
                              activeColor: customColor().logoColor,

                              value: bookMasterList[i].okrMaster
                                  .isActive != null ? bookMasterList[i].okrMaster
                                  .isActive : false,
                              onChanged: (bool value) {
                                setState(() {

                                  bookMasterList[i].okrMaster.isActive =
                                      value;
                                  if(value == true)
                                  {
                                    okrMasterTempList.add(bookMasterList[i].okrMaster);
                                  }
                                  //else{
                                  //   deleteBookmark( postMasterAllTestList[i].postId);
                                  // }

                                });
                              },
                            ),
                            Expanded(
                              flex: 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(text: TextSpan(
                                    children: [
                                      WidgetSpan(
                                        child: Text("O :   ",
                                          style: GoogleFonts.montserrat(
                                            fontSize: 15,
                                            letterSpacing: 1,
                                            color: customColor().barColor,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),

                                      ),

                                      TextSpan(
                                        text: bookMasterList[i].okrMaster.objective,
                                        style: GoogleFonts.montserrat(
                                                    fontSize: 11,
                                                    letterSpacing: 1.0,
                                                    color: customColor().barColor,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                      ),
                                    ],
                                  ),
                                  ),
                                  SizedBox(height: 10.0,),
                                  RichText(text: TextSpan(
                                    children: [
                                      WidgetSpan(
                                        child: Text("KR : ",
                                          style: GoogleFonts.montserrat(
                                            fontSize: 14,
                                            letterSpacing: 1.0,
                                            color: customColor().barColor,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),

                                      ),

                                      TextSpan(
                                        text: bookMasterList[i].okrMaster.keyResult,
                                        style: GoogleFonts.montserrat(
                                          fontSize: 10,
                                          letterSpacing: 1.0,
                                          color: customColor().barColor,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ),
                                ],
                              ),
                            ),
                            Container(child: IconButton(icon: Icon(Icons.star, color: customColor().logoColor), onPressed: (){
                                deleteBookmark(bookMasterList[i].bookmarkId);
                            })),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.all(15.0),
          //   child: CustomBox(),
          // ),
          // Padding(
          //   padding: const EdgeInsets.all(15.0),
          //   child: CustomBox(),
          // ),
        ],

      ),
    );
  }
  updateSearchState(String text) {
    // latestId = 0;
    // defaultRecordSize = 100;
    if (text.isNotEmpty) {
      setState(() {
        bookMasterList = [];
        latestId = 0;
        bookmarkList();
      });
    }
    else if(text.isEmpty){
      setState(() {
        latestId = 0;
        bookmarkList();
      });
    }
  }
  void bookmarkList() async{
    // if (prevSearched != null && prevSearched != txtsearch.text) {
    //   bookMasterList = [];
    // }
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    User userTemp = new User(id: user.id);
    print(user.id);
    String search = txtsearch.text;
    OKRMaster okrMaster = new OKRMaster();
    BookmarkMaster bookmarkMaster = BookmarkMaster(currentUser: userTemp, pageSize: defaultRecordSize, pageNumber: latestId);
    if(txtsearch.text.isNotEmpty){
      okrMaster.searchValue = txtsearch.text.toString();
      bookmarkMaster.okrMaster = okrMaster;
    }
    var body = json.encode(bookmarkMaster);
    var jsonResponse;
    var url = Uri.parse(AppConstants.BookMarkList_URL);
    var response = await http.post(url,
        headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if(jsonResponse.length > 0) {
        prevSearched = okrMaster.searchValue;
        List<BookmarkMaster> bookList =
        jsonResponse.map<BookmarkMaster>((json) => BookmarkMaster.fromJson(json))
            .toList();
       // bookMasterList = [];
       
        bookMasterList.addAll(bookList);
        setState(()
        {});
      }
    }
    else
    {
      ShowToast().showToast(context, "Could'nt Connect to Server");
    }

  }
  void deleteBookmark(int bookmarkId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    BookmarkMaster bookmarkMaster = new BookmarkMaster(bookmarkId: bookmarkId);
    var body = json.encode(bookmarkMaster);
    String call;
    var url = Uri.parse(AppConstants.DeleteBookMark_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      call = response.body;
      if (call.trim() == "SUCCESS") {

          int   id = bookmarkMaster.bookmarkId;
          bookMasterList.removeWhere((item) => item.bookmarkId == id);
        ShowToast().showToast(context, "Removed from bookmark");
        setState(() {
        });
      }
    }
  }
  Future<void> sendDownload(DownloadItem downloadItem) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    var body = json.encode(downloadItem);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Download_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        print(jsonResponse['errorMessage']);
        if(jsonResponse['errorMessage'] != null)
        {
          if(jsonResponse['errorMessage'] == "ALREADY-MAX-SIZE")
          {
            ShowToast().showToast(context, "You have exceeded your daily download limit");
          }
          else if(jsonResponse['errorMessage'] == "MORE-THEN-MAX-SIZE")
          {
            ShowToast().showToast(context, "You have exceeded your daily download limit");
          }
          else if(jsonResponse['errorMessage'] == "SUCCESS")
          {
            ShowToast().showToast(context, "You will receive an e-mail");
          }else{
            ShowToast().showToast(context, "Try again later");
          }
        }else{
          ShowToast().showToast(context, "Try again later");
        }

      }else{
        ShowToast().showToast(context, "Network Issue");
      }
    }
  }

}
