import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/widget/app_bar.dart';

class Profile extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Profile",)),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                    width: 190.0,
                    height: 190.0,
                    decoration: new BoxDecoration(
                      color: Colors.black,
                        shape: BoxShape.circle,
                        // image: new DecorationImage(
                        //     fit: BoxFit.fill,
                        //     image: new NetworkImage(
                        //         "https://i.imgur.com/BoN9kdC.png")
                        // )
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
