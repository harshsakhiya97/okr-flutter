import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/home/search_result.dart';
import 'package:okr/services/models/OKRMaster.dart';
import 'package:okr/services/models/SaveMaster.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/category.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/app_drawer.dart';
import 'package:okr/widget/colorCustom.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  OKRMaster okrMaster;
  User user;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
  }

  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {

        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Home",)),
      drawer: AppDrawer(),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(


                child: Stack(

                  children: <Widget>[
                    SizedBox(height: 20.0,),
                    SizedBox(height: 50,),
                    Container(
                      padding:  EdgeInsets.only(top: 225,left: 25,right: 25),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 50,
                            decoration: BoxDecoration(
                                border: Border.all(

                                    color: Colors.grey,
                                    style: BorderStyle.solid,
                                    width: 1.0),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(40.0)),
                            child: Column(
                              children: <Widget>[

                                GestureDetector(

                                  child: Padding(
                                    padding: EdgeInsets.only(left: 0.0,),

                                    child: TextField(
                                      readOnly: true,
                                      onTap: (){MyNavigator.goToAllTestScreen(context);},
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Search for OKRs',
                                        hintStyle: TextStyle(
                                          fontSize: 13,
                                          fontFamily: 'Montserrat',
                                          color: Colors.grey,

                                        ),
                                        icon:
                                        IconButton(
                                          icon: Icon(Icons.search, size: 15,),
                                          onPressed: (){
                                            // MyNavigator.goToSearch(context);
                                          },
                                        ),
                                      ),
                                    ),

                                  ),
                                  // onTap: (){MyNavigator.goToSearch(context);},
                                ),
                              ],
                            ),

                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 30, bottom: 5, left: 25, right: 25),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),color: customColor().logoColor,), child: Padding(
                          padding: const EdgeInsets.only(left: 12, top: 12, bottom: 15, right: 18),
                          child: IconButton(icon: Icon(Icons.search,size: 40,color: Colors.white,), onPressed: (){MyNavigator.goToFilter(context);}),
                        )),

                        Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),color: customColor().logoColor,), child: Padding(
                          padding: const EdgeInsets.only(left: 12, top: 12, bottom: 15, right: 18),
                          child: IconButton(icon: Icon(Icons.saved_search,size: 40,color: Colors.white), onPressed: (){MyNavigator.goToSearch(context);}),
                        ),),

                        Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),color: customColor().logoColor,), child: Padding(
                          padding: const EdgeInsets.only(left: 12, top: 12, bottom: 15, right: 18),
                          child: IconButton(icon: Icon(Icons.business_outlined, size: 40,color: Colors.white), onPressed: () async {
                            user = User.fromJson(await SharedPref().read("user"));
                            if(user != null && user.departmentId != null && user.departmentId != 0)
                           {
                             SaveMaster saveMasterResp = new SaveMaster();
                             Category category = new Category(predefinedId: user.departmentId);
                             saveMasterResp.department = category;
                             Navigator.push(
                               context,
                               MaterialPageRoute(
                                 builder: (_) => SearchResult(
                                   saveMaster: saveMasterResp,
                                 ),),);
                           }else {
                           ShowToast().showToast(context,
                               "Please Select Department in my Profile");
                         }
                          }),
                        ),),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),

        ),
      ),
    );
  }

}
