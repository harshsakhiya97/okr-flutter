import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okr/services/models/SaveMaster.dart';
import 'package:okr/services/models/category.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/custom_box.dart';
import 'package:okr/widget/custom_save.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/OKRMaster.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/bookmarkMaster.dart';
import 'package:okr/services/models/postMaster.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_box.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

import 'search_result.dart';

class SaveSearch extends StatefulWidget {

  @override
  _SaveSearchState createState() => _SaveSearchState();
}

class _SaveSearchState extends State<SaveSearch> {
  User user;
  List<SaveMaster> saveMasterList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
    saveSearchList();
  }

  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Saved Search",)),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child:  ListView.builder(
                scrollDirection: Axis.vertical,
                physics: ScrollPhysics(),
                //controller: _scrollController,
                shrinkWrap: true,
                itemCount: saveMasterList.length,
                itemBuilder: (context, i) =>
                Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: new Column(
                      children: <Widget>[
                  RawMaterialButton(
                    child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                      color: Colors.grey,
                      style: BorderStyle.solid,
                    ),

                      borderRadius: BorderRadius.circular(10.0)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        saveMasterList[i].searchName != null && saveMasterList[i].searchName.isNotEmpty ?
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Text("Keyword : " + saveMasterList[i].searchName,
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              color: customColor().barColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ):Container(),
                        saveMasterList[i].departmentName != null && saveMasterList[i].departmentName.isNotEmpty ?
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text("Category : " +  saveMasterList[i].departmentName,
                          style: GoogleFonts.montserrat(
                            fontSize: 16,
                            color: customColor().barColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        ):Container(),
                        saveMasterList[i].departmentName != null && saveMasterList[i].subDepartmentName.isNotEmpty  ?
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text("Subcategory : " + saveMasterList[i].subDepartmentName,
                            style: GoogleFonts.montserrat(
                              fontSize: 16,
                              color: customColor().barColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ):Container(),
                      ],
                    ),
              ),
              Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Container(
                          decoration: BoxDecoration(
                              color: customColor().logoColor,
                              // border: Border.all(
                              //   style: BorderStyle.solid,
                              // ),

                              borderRadius: BorderRadius.circular(40.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: Text(saveMasterList[i].okrCounts.toString(),
                              style: GoogleFonts.montserrat(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(child: IconButton(icon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(Icons.delete_forever_outlined, color: customColor().logoColor),
                      ), onPressed: (){
                        deleteSaveSearchMaster(saveMasterList[i].savedSearchMasterId);
                      })),
                    ],
              ),
            ],
          ),
        ),
      ),
                    onPressed: (){
                      SaveMaster saveMasterResp = saveMasterList[i];
                      if(saveMasterList[i].departmentId !=null && saveMasterList[i].departmentId != 0)
                        {
                          Category category = new Category(predefinedId: saveMasterList[i].departmentId);
                          saveMasterResp.department = category;
                        }
                      if(saveMasterList[i].subDepartmentId !=null && saveMasterList[i].subDepartmentId != 0)
                      {
                        Category category = new Category(predefinedId: saveMasterList[i].subDepartmentId);
                        saveMasterResp.subDepartment = category;
                      }
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => SearchResult(
                            saveMaster: saveMasterResp,
                          ),),);
                    },
                  ),
                  ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void saveSearchList() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    User userTemp = new User(id: user.id);
    SaveMaster saveMaster = SaveMaster(createdBy: userTemp);
    var body = json.encode(saveMaster);
    var jsonResponse;
    var url = Uri.parse(AppConstants.SaveSearchList_URL);
    var response = await http.post(url,
        headers: {HttpHeaders.contentTypeHeader: "application/json", HttpHeaders.authorizationHeader: "Bearer $token"},
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if(jsonResponse.length > 0) {
        List<SaveMaster> saveList =
        jsonResponse.map<SaveMaster>((json) => SaveMaster.fromJson(json))
            .toList();
        saveMasterList.addAll(saveList);
        setState(() {});
      }
    }
    else
    {
      ShowToast().showToast(context, "Could'nt Connect to Server");
    }

  }

  void deleteSaveSearchMaster(int saveMasterId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    SaveMaster saveMaster = new SaveMaster(savedSearchMasterId: saveMasterId);
    var body = json.encode(saveMaster);
    String call;
    var url = Uri.parse(AppConstants.DeleteSaveSearch_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      call = response.body;
      if (call.trim() == "SUCCESS") {

        int   id = saveMaster.savedSearchMasterId;
        saveMasterList.removeWhere((item) => item.savedSearchMasterId == id);
        ShowToast().showToast(context, "Removed from Save Search");
        setState(() {
        });
      }
    }
  }
}
