import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/UserDto.dart';
import 'package:okr/services/models/category.dart';
import 'package:okr/services/models/subcategory.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/utils/my_navigator.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/custom_button.dart';
import 'package:okr/widget/text_field.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfile extends StatefulWidget {
  final User user;
  const EditProfile({Key key, this.user})
      : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final TextEditingController firstName = TextEditingController();
  final TextEditingController lastName = TextEditingController();
  final TextEditingController industryName = TextEditingController();
  final TextEditingController designation = TextEditingController();
  final TextEditingController designationLevel = TextEditingController();
  final TextEditingController department = TextEditingController();
  final TextEditingController region = TextEditingController();
  final TextEditingController company = TextEditingController();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  String sort = "asc";
  int categoryId = 0, subcategoryId = 0, industryId = 0, desId = 0, desLevel = 0, regionId = 0;
  List<SubCategory> subCategoryListTemp = [];
  Category currentCategory, currentIndustry, currentDes, currentDesLevel, currentRegion ;
  SubCategory currentSubCategory ;

  get keyController => null;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCategories();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),child: CustomAppBar(text: "Edit Profile",)),

      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              // height: Platform.isIOS ? 62 : 56,
              // height: 56.0,
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.only(left: 25,right: 25 , bottom: 10, top: 20),
                child: AMTextField(text: widget.user.firstName.isNotEmpty ? widget.user.firstName : "First Name",
                  controller: firstName,
                  readOnly: false,
                  validator: (value) {
                    if (value.length < 3)
                      return 'Name must be more than 2 charater';
                    else
                      return null;
                  },
                  //  onSaved: (val) => signname = val,
                  obscureText: false,
                ),
              ),
            ),
            Container(
              // height: Platform.isIOS ? 62 : 56,
              // height: 56.0,
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.only(left: 25,right: 25 , top: 15),
                child: AMTextField(text: widget.user.lastName.isNotEmpty ? widget.user.lastName : "Last Name",
                  controller: lastName,
                  readOnly: false,
                  validator: (value) {
                    if (value.length < 3)
                      return 'Name must be more than 2 charater';
                    else
                      return null;
                  },
                  //  onSaved: (val) => signname = val,
                  obscureText: false,
                ),
              ),
            ),
            Container(
              // height: Platform.isIOS ? 62 : 56,
              // height: 56.0,
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.only(left: 25,right: 25 , top: 15),
                child: AMTextField(text: widget.user.companyName.isNotEmpty ? widget.user.companyName : "Company Name",
                  controller: company,
                  readOnly: false,
                  validator: (value) {
                    if (value.length < 2)
                      return 'Name must be more than 2 charater';
                    else
                      return null;
                  },
                  //  onSaved: (val) => signname = val,
                  obscureText: false,
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                child: Center(
                  child: Container(
                    height: 56.0,
                    width: 500.0,
                    decoration: BoxDecoration(
                        color: customColor().logoColor,

                        border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                            width: 1.0),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Column(
                      //   crossAxisAlignment:  CrossAxisAlignment.center,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[



                        Expanded(
                          child: FutureBuilder<List<Category>>(
                              future: getIndustry(),
                              builder: (BuildContext context,
                                  categoryList) {
                                if (!categoryList.hasData) return CircularProgressIndicator();
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton<Category>(
                                    iconEnabledColor: Colors.white,
                                    iconDisabledColor: Colors.white,
                                    icon: Center(child: Icon(Icons.arrow_drop_down)),
                                    items: categoryList.data
                                        .map((category) => DropdownMenuItem<Category>(
                                      child: Text(category.name),

                                      value: category,
                                    ))
                                        .toList(),
                                    onChanged: (Category value) {
                                      setState(() {
                                        currentIndustry = value;
                                        widget.user.industryName = currentIndustry.name;
                                        // industryId = c.predefinedId;
                                      });
                                    },
                                    isExpanded: true,
                                    //  value: currentCategory,
                                    hint: widget.user.industryName.isNotEmpty ? Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text(widget.user.industryName,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ) : Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text("Select Industry",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ),

                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                child: Center(
                  child: Container(
                    height: 56.0,
                    width: 500.0,
                    decoration: BoxDecoration(
                        color: customColor().logoColor,

                        border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                            width: 1.0),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Column(
                      //   crossAxisAlignment:  CrossAxisAlignment.center,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[



                        Expanded(
                          child: FutureBuilder<List<Category>>(
                              future: getDesignation(),
                              builder: (BuildContext context,
                                  categoryList) {
                                if (!categoryList.hasData) return CircularProgressIndicator();
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton<Category>(
                                    iconEnabledColor: Colors.white,
                                    iconDisabledColor: Colors.white,
                                    icon: Center(child: Icon(Icons.arrow_drop_down)),
                                    items: categoryList.data
                                        .map((category) => DropdownMenuItem<Category>(
                                      child: Text(category.name),

                                      value: category,
                                    ))
                                        .toList(),
                                    onChanged: (Category value) {
                                      setState(() {
                                        currentDes = value;
                                        widget.user.designationName = currentDes.name;
                                      });
                                    },
                                    isExpanded: true,
                                    //  value: currentCategory,
                                    hint: widget.user.designationName.isNotEmpty ? Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text(widget.user.designationName,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ) : Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text("Select Designation",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ),

                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                child: Center(
                  child: Container(
                    height: 56.0,
                    width: 500.0,
                    decoration: BoxDecoration(
                        color: customColor().logoColor,

                        border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                            width: 1.0),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Column(
                      //   crossAxisAlignment:  CrossAxisAlignment.center,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[



                        Expanded(
                          child: FutureBuilder<List<Category>>(
                              future: getDesignationLevel(),
                              builder: (BuildContext context,
                                  categoryList) {
                                if (!categoryList.hasData) return CircularProgressIndicator();
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton<Category>(
                                    iconEnabledColor: Colors.white,
                                    iconDisabledColor: Colors.white,
                                    icon: Center(child: Icon(Icons.arrow_drop_down)),
                                    items: categoryList.data
                                        .map((category) => DropdownMenuItem<Category>(
                                      child: Text(category.name),

                                      value: category,
                                    ))
                                        .toList(),
                                    onChanged: (Category value) {
                                      setState(() {
                                        currentDesLevel = value;
                                        widget.user.designationLevelName = currentDesLevel.name;
                                      });
                                    },
                                    isExpanded: true,
                                    //  value: currentCategory,
                                    hint: widget.user.designationLevelName.isNotEmpty ? Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text(widget.user.designationLevelName,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ) : Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text("Select Designation-Level",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ),

                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                child: Center(
                  child: Container(
                    height: 56.0,
                    width: 500.0,
                    decoration: BoxDecoration(
                        color: customColor().logoColor,

                        border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                            width: 1.0),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Column(
                      //   crossAxisAlignment:  CrossAxisAlignment.center,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[



                        Expanded(
                          child: FutureBuilder<List<Category>>(
                              future: getRegion(),
                              builder: (BuildContext context,
                                  categoryList) {
                                if (!categoryList.hasData) return CircularProgressIndicator();
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton<Category>(
                                    iconEnabledColor: Colors.white,
                                    iconDisabledColor: Colors.white,
                                    icon: Center(child: Icon(Icons.arrow_drop_down)),
                                    items: categoryList.data
                                        .map((category) => DropdownMenuItem<Category>(
                                      child: Text(category.name),

                                      value: category,
                                    ))
                                        .toList(),
                                    onChanged: (Category value) {
                                      setState(() {
                                        currentRegion = value;
                                        widget.user.regionName = currentRegion.name;
                                      });
                                    },
                                    isExpanded: true,
                                    //  value: currentCategory,
                                    hint: widget.user.regionName.isNotEmpty ? Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text(widget.user.regionName,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ) : Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text("Select Region",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ),

                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                  child: Center(
                    child: Container(
            height: 56.0,
            width: 500.0,
            decoration: BoxDecoration(
                color: customColor().logoColor,

                border: Border.all(
                    color: Colors.grey,
                    style: BorderStyle.solid,
                    width: 1.0),

                borderRadius: BorderRadius.circular(40.0)),
            child: Column(
              //   crossAxisAlignment:  CrossAxisAlignment.center,
              //  mainAxisSize: MainAxisSize.max,
              children: <Widget>[



                Expanded(
                  child: FutureBuilder<List<Category>>(
                      future: getCategories(),
                      builder: (BuildContext context,
                          categoryList) {
                        if (!categoryList.hasData) return CircularProgressIndicator();
                        return DropdownButtonHideUnderline(
                          child: DropdownButton<Category>(
                            iconEnabledColor: Colors.white,
                            iconDisabledColor: Colors.white,
                            icon: Center(child: Icon(Icons.arrow_drop_down)),
                            items: categoryList.data
                                .map((category) => DropdownMenuItem<Category>(
                              child: Text(category.name),

                              value: category,
                            ))
                                .toList(),
                            onChanged: (Category value) {
                              setState(() {
                                currentCategory = value;
                                widget.user.departmentName = currentCategory.name;
                                categoryId = currentCategory.predefinedId;
                                currentSubCategory = null;
                               // getSubCategories();
                              });
                            },
                            isExpanded: true,
                            //  value: currentCategory,
                            hint: widget.user.departmentName.isNotEmpty ? Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Center(child: Text(widget.user.departmentName,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                            ) : Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Center(child: Text("Select Department",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                            ),

                          ),
                        );
                      }),
                ),

              ],
            ),
                    ),
                  ),
                ),
              ),
            SizedBox(height: 10,),
        Padding(
          padding: const EdgeInsets.only(left: 25,right: 25 , top: 15),
          child: CustomButton(
                      onPresed: (){
                       saveSearch();
                       // submit();
                      },
                      text: "Save",
                    ),
        ),
                  SizedBox(height: 20.0,),
//             Padding(
//               padding: const EdgeInsets.only(left: 30.0, right: 30.0),
//               child: Center(
//                 child: Container(
//                   height: 56.0,
//                   width: 500.0,
//                   decoration: BoxDecoration(
//                       color: customColor().logoColor,
//                       // gradient: LinearGradient(colors: [customColor().barColor, customColor().bkColor],
//                       //   begin: Alignment.centerLeft,
//                       //   end: Alignment.centerRight,
//                       // ),
//                       border: Border.all(
//                           color: Colors.grey,
//                           style: BorderStyle.solid,
//                           width: 1.0),
//
//                       borderRadius: BorderRadius.circular(40.0)
//
//                   ),
//                   child: Column(
//                     children: <Widget>[
//                       Expanded(
//                         child: FutureBuilder<List<SubCategory>>(
//                             future: getSubCategories(),
//                             builder: (BuildContext context,
//                                 AsyncSnapshot<List<SubCategory>> snapshot) {
//                               if (!snapshot.hasData) return Center(child: Padding(
//                                 padding: const EdgeInsets.only(left: 20.0),
//                                 child: Text("Select Subcategory",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white,)),
//                               ));
//                               return DropdownButtonHideUnderline(
//                                 child: DropdownButton<SubCategory>(
//                                   iconEnabledColor: Colors.white,
//                                   iconDisabledColor: Colors.white,
//                                   icon: Center(child: Icon(Icons.arrow_drop_down)),
//                                   items: snapshot.data
//                                       .map((subcategory) => DropdownMenuItem<SubCategory>(
//                                     child: Text(subcategory.name),
//
//                                     value: subcategory,
//                                   ))
//                                       .toList(),
//                                   onChanged: (SubCategory value) {
//                                     setState(() {
//                                       currentSubCategory = value;
//                                       subcategoryId = currentSubCategory.predefinedId;
//
//                                       //getSubCategory();
//                                     });
//                                   },
//                                   isExpanded: true,
//                                   //  value: currentCategory,
//                                   hint: currentSubCategory != null ? Padding(
//                                     padding: const EdgeInsets.only(left: 20.0),
//                                     child: Center(child: Text(currentSubCategory.name,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
//                                   ) : Padding(
//                                     padding: const EdgeInsets.only(left: 20.0),
//                                     child: Center(child: Text("Select Sub-Category",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
//                                   ),
//
//                                 ),
//                               );
//                             }),
//                       ),
//                     ],
//
// //                  ),
// //                     ],
//                   ),
//
//                 ),
//               ),
//
//             ),
          ],
        ),
      ),

    );
  }
  Future<List<Category>> getCategories() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("DEPARTMENT");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }

  Future<List<Category>> getIndustry() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("INDUSTRY");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }
  Future<List<Category>> getDesignation() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("DESIGNATION");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }
  Future<List<Category>> getDesignationLevel() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("DESIGNATION-LEVEL");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }
  Future<List<Category>> getRegion() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("REGION");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }

  // Future<List<SubCategory>> getSubCategories() async {
  //   String sort = "asc";
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   String token = sharedPreferences.getString("token");
  //   Category category = new Category(parentPredefinedId: categoryId);
  //   // category.setSortOrder(sort);
  //   category.setEntityType("SUB-DEPARTMENT");
  //   var body = json.encode(category);
  //   var jsonResponse;
  //   var url = Uri.parse(AppConstants.Category_URL);
  //   var response = await http.post(url,
  //       headers: {
  //         HttpHeaders.contentTypeHeader: "application/json",
  //         HttpHeaders.authorizationHeader: "Bearer $token"
  //       },
  //       body: body);
  //
  //   if (response.statusCode == 200) {
  //     jsonResponse = json.decode(response.body);
  //     if (jsonResponse.length > 0) {
  //       List<SubCategory> subCategoryList =
  //       jsonResponse.map<SubCategory>((json) => SubCategory.fromJson(json))
  //           .toList();
  //       subCategoryListTemp = [];
  //       subCategoryListTemp.addAll(subCategoryList);
  //       return subCategoryListTemp;
  //     }
  //   }
  // }

  void saveSearch() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
   // User userTemp = new User(id: widget.user.id);
    User userResp = widget.user;
    Category categoryResp = new Category();
    if(firstName.text.isNotEmpty)
    {
      userResp.firstName = firstName.text.toString();
    }
    if(lastName.text.isNotEmpty)
    {
      userResp.lastName = lastName.text.toString();
    }
    if(company.text.isNotEmpty)
    {
      userResp.companyName = company.text.toString();
    }
    if(currentCategory != null)
    {
      userResp.department = currentCategory;
    }
    if(currentRegion != null)
    {
      userResp.region = currentRegion;
    }
    if(currentDesLevel != null)
    {
      userResp.designationLevel = currentDesLevel;
    }
    if(currentDes != null)
    {
      userResp.designation = currentDes;
    }
    if(currentIndustry != null)
    {
      userResp.industry = currentIndustry;
    }
    try {
    var body = json.encode(userResp);
    String duplicateError;
    var url = Uri.parse(AppConstants.UpdateUser_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      duplicateError = response.body;
      if (duplicateError.toString().isNotEmpty && duplicateError.toString() == 'SUCCESS') {
        getuserDetail(userResp);
      }else{
        ShowToast().showToast(context, "Please try again");
      }
    }
    } on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }

  void getuserDetail(User userDto) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    User user = User(username: userDto.username);
    var body = json.encode(user);
    var jsonResponse;
    try {
      var url = Uri.parse(AppConstants.UserDetail_URL);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: body);
      if (response.statusCode == 200) {
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          SharedPref().save("user", jsonResponse);
          ShowToast().showToast(context, "User Updated Successfully");
          MyNavigator.goToMyProfile(context);
        }
        else {
          ShowToast().showToast(context, "Something went wrong");
        }
      }
      else {
        ShowToast().showToast(context, "Server Problem");
      }
    } on Exception catch (_) {
      ShowToast().showToast(context, "Check Your Internet Connection");
    }
  }
  loadSharedPrefs() async {
    try {
      User user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }
}
