import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okr/home/search_result.dart';
import 'package:okr/services/appconstant.dart';
import 'package:okr/services/models/SaveMaster.dart';
import 'package:okr/services/models/User.dart';
import 'package:okr/services/models/category.dart';
import 'package:okr/services/models/subcategory.dart';
import 'package:okr/services/share_pref.dart';
import 'package:okr/services/show_toast.dart';
import 'package:okr/widget/app_bar.dart';
import 'package:okr/widget/colorCustom.dart';
import 'package:okr/widget/text_field.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

class AdvanceSearch extends StatefulWidget {

  @override
  _AdvanceSearchState createState() => _AdvanceSearchState();
}

class _AdvanceSearchState extends State<AdvanceSearch> {
  bool isFavorite = false;
//  var _star = new Icon(Icons.ar);
  String sort = "asc";
  int categoryId = 0, subcategoryId = 0;
  String parName = "";
  User user;
  // var i;
//  List<Category> categoryList = List();
  final TextEditingController keyController = new TextEditingController();

  List<SubCategory> subCategoryListTemp = [];
  Category currentCategory ;
  SubCategory currentSubCategory ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSharedPrefs();
    getCategories();
  }

  loadSharedPrefs() async {
    try {
      user = User.fromJson(await SharedPref().read("user"));
      setState(() {
        print(user);
      });
    } catch (Excepetion) {
      // do something
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight),
          child: CustomAppBar(text: "Advance Search",)),

      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 100, right: 30, left: 30),
              child: AMTextField(text: "Keyword", obscureText: false, controller: keyController,readOnly: false,),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, right: 30, left: 30),
                child: Center(
                  child: Container(
                    height: 56.0,
                    width: 500.0,
                    decoration: BoxDecoration(
                      color: customColor().logoColor,
                        
                        border: Border.all(
                            color: Colors.grey,
                            style: BorderStyle.solid,
                            width: 1.0),

                        borderRadius: BorderRadius.circular(40.0)),
                    child: Column(
                      //   crossAxisAlignment:  CrossAxisAlignment.center,
                      //  mainAxisSize: MainAxisSize.max,
                      children: <Widget>[



                        Expanded(
                          child: FutureBuilder<List<Category>>(
                              future: getCategories(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<List<Category>> snapshot) {
                                if (!snapshot.hasData) return CircularProgressIndicator();
                                return DropdownButtonHideUnderline(
                                  child: DropdownButton<Category>(
                                    iconEnabledColor: Colors.white,
                                    iconDisabledColor: Colors.white,
                                    icon: Center(child: Icon(Icons.arrow_drop_down)),
                                    items: snapshot.data
                                        .map((category) => DropdownMenuItem<Category>(
                                      child: Text(category.name),

                                      value: category,
                                    ))
                                        .toList(),
                                    onChanged: (Category value) {
                                      setState(() {
                                        currentCategory = value;
                                        categoryId = currentCategory.predefinedId;
                                        currentSubCategory = null;
                                       getSubCategories();
                                      });
                                    },
                                    isExpanded: true,
                                    //  value: currentCategory,
                                    hint: currentCategory != null ? Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text(currentCategory.name,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ) : Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: Center(child: Text("Select Category",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                    ),

                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.0,),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 30.0),
              child: Center(
                child: Container(
                  height: 56.0,
                  width: 500.0,
                  decoration: BoxDecoration(
                    color: customColor().logoColor,
                    // gradient: LinearGradient(colors: [customColor().barColor, customColor().bkColor],
                    //   begin: Alignment.centerLeft,
                    //   end: Alignment.centerRight,
                    // ),
                      border: Border.all(
                          color: Colors.grey,
                          style: BorderStyle.solid,
                          width: 1.0),

                      borderRadius: BorderRadius.circular(40.0)

                  ),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: FutureBuilder<List<SubCategory>>(
                            future: getSubCategories(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<SubCategory>> snapshot) {
                              if (!snapshot.hasData) return Center(child: Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: Text("Select Subcategory",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white,)),
                              ));
                              return DropdownButtonHideUnderline(
                                child: DropdownButton<SubCategory>(
                                  iconEnabledColor: Colors.white,
                                  iconDisabledColor: Colors.white,
                                  icon: Center(child: Icon(Icons.arrow_drop_down)),
                                  items: snapshot.data
                                      .map((subcategory) => DropdownMenuItem<SubCategory>(
                                    child: Text(subcategory.name),

                                    value: subcategory,
                                  ))
                                      .toList(),
                                  onChanged: (SubCategory value) {
                                    setState(() {
                                      currentSubCategory = value;
                                      subcategoryId = currentSubCategory.predefinedId;

                                      //getSubCategory();
                                    });
                                  },
                                  isExpanded: true,
                                  //  value: currentCategory,
                                  hint: currentSubCategory != null ? Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Center(child: Text(currentSubCategory.name,style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                  ) : Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Center(child: Text("Select Sub-Category",style: TextStyle(fontSize: 20, fontFamily: "Montserrat", color: Colors.white),)),
                                  ),

                                ),
                              );
                            }),
                      ),
                    ],

//                  ),
//                     ],
                  ),

                ),
              ),

            ),
            //     Center(child: Icon(Icons.arrow_drop_down,color: Colors.white,)),

            SizedBox(height: 40.0,)

          ],
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.only(bottom: 2.0, right: 2.0, left: 5.0),
              height: 40.0,
              //   width: 200.0,
              child: RawMaterialButton(
                onPressed: () {
                  setState(() {
                    if(keyController.text.isNotEmpty)
                      {
                        saveSearch(keyController.text.toString());
                      }else{
                      ShowToast().showToast(context, "Keyword should not be empty");
                    }
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: customColor().logoColor,
                      border: Border.all(
                          color: Colors.grey,
                          style: BorderStyle.solid,
                          width: 1.0),

                      borderRadius: BorderRadius.circular(10.0)),
                  child: Center(

                    child: Text(
                      'Save & Search',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(bottom: 2.0, right: 5.0),
              height: 40.0,
              //   width: 200.0,
              child: RawMaterialButton(
                onPressed: () {
                  if(keyController.text.isNotEmpty)
                  {
                    SaveMaster saveMaster = SaveMaster(searchName: keyController.text.toString());
                    if(currentCategory != null && currentCategory.predefinedId != null)
                    {
                      Category category = new Category();
                      category.predefinedId = currentCategory.predefinedId;
                      saveMaster.department = category;
                    }
                    if(currentSubCategory != null && currentSubCategory.predefinedId != null)
                    {
                      Category category = new Category();
                      category.predefinedId = currentSubCategory.predefinedId;
                      saveMaster.subDepartment = category;
                    }
                    Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => SearchResult(
                              saveMaster: saveMaster,
                            ),),);
                  }else{
                    ShowToast().showToast(context, "Keyword should not be empty");
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: customColor().logoColor,
                      border: Border.all(
                          color: Colors.grey,
                          style: BorderStyle.solid,
                          width: 1.0),

                      borderRadius: BorderRadius.circular(10.0)),
                  child: Center(
                    child: Text(
                      'Search',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  Future<List<Category>> getCategories() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category();
    // category.setSortOrder(sort);
    category.setEntityType("DEPARTMENT");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<Category> categoryList =
        jsonResponse.map<Category>((json) => Category.fromJson(json))
            .toList();
        return categoryList;
      }
    }
  }

  Future<List<SubCategory>> getSubCategories() async {
    String sort = "asc";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    Category category = new Category(parentPredefinedId: categoryId);
    // category.setSortOrder(sort);
    category.setEntityType("SUB-DEPARTMENT");
    var body = json.encode(category);
    var jsonResponse;
    var url = Uri.parse(AppConstants.Category_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        List<SubCategory> subCategoryList =
        jsonResponse.map<SubCategory>((json) => SubCategory.fromJson(json))
            .toList();
        subCategoryListTemp = [];
        subCategoryListTemp.addAll(subCategoryList);
        return subCategoryListTemp;
      }
    }
  }

  void saveSearch(String keyword) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("token");
    User userTemp = new User(id: user.id);
    SaveMaster saveMaster = SaveMaster(createdBy: userTemp, searchName: keyword);
    if(currentCategory != null && currentCategory.predefinedId != null)
      {
        Category category = new Category();
        category.predefinedId = currentCategory.predefinedId;
        saveMaster.department = category;
      }
    if(currentSubCategory != null && currentSubCategory.predefinedId != null)
    {
      Category category = new Category();
      category.predefinedId = currentSubCategory.predefinedId;
      saveMaster.subDepartment = category;
    }
    var body = json.encode(saveMaster);
    var jsonResponse;
    var url = Uri.parse(AppConstants.SaveSearch_URL);
    var response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        },
        body: body);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse.length > 0) {
        ShowToast().showToast(context, "Saved Search Successfully");
        currentSubCategory = null;
        currentCategory = null;
        keyController.clear();
        subCategoryListTemp.clear();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => SearchResult(
              saveMaster: saveMaster,
            ),),);


        setState(() {
        });
      }
    }
  }

}

